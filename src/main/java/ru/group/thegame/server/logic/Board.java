/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic;

import java.util.List;
import ru.group.thegame.server.logic.state.BoardState;

/**
 *
 * @author Nevezhin_Pavel
 */
public class Board {

	private final int maxCardsCount = 6;

	private final BoardState boardState;

	public Board() {
		boardState = new BoardState(maxCardsCount);
	}

	public int getMaxCardsCount() {
		return maxCardsCount;
	}

	public BoardState getBoardState() {
		return boardState;
	}

	/**
	 * <p>
	 * Положить карту</p>
	 * <p>
	 * Проверяет свободна ли позиция, на которую хотят положить карту, в случает
	 * если ячейка пуста карта кладется.
	 * </p>
	 *
	 * @param card Объект карты, которую положили
	 * @return Возвращает true если карта успешно добавлена, иначе false
	 */
	public boolean putCard(Card card) {
		List<Card> userCards = boardState.getUsersCards().get(card.getOwner());
		if (userCards.get(card.getCardState().getPosition()) == null) {
			userCards.set(card.getCardState().getPosition(), card);
			return true;
		}
		return false;
	}

	/**
	 * <p>
	 * Удалить карту</p>
	 * <p>
	 * Проверяет есть ли карта на этой позиции, откуда хотят удалить карту, в
	 * случает если ячейка не пуста карта удаляется.
	 * </p>
	 *
	 * @param player Игрок, чья карта удаляется
	 * @param position Позиция, откуда карта удаляется
	 * @return Возвращает true если карта успешно удалена, иначе false.
	 */
	public boolean removeCard(int player, int position) {
		List<Card> userCards = boardState.getUsersCards().get(player);
		if (userCards.get(position) != null){
			userCards.set(position, null);
		}
		return false;
	}

	/**
	 * <p>
	 * Получит список карт игрока</p>
	 * <p>
	 * В зависимости от переданного номера игрока возвращается список
	 * </p>
	 *
	 * @param user Номер игрока, чьи карты требуется получить
	 * @return List< Card > список карт
	 */
	public List<Card> getUserCards(int user) {
		List<Card> userCards = boardState.getUsersCards().get(user);
		return userCards;
	}
}
