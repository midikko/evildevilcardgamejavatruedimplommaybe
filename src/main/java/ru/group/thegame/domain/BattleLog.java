/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.domain;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author potapov
 */
@Entity
@Table(name = "battle_log")
public class BattleLog implements Serializable {
    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
        
    @ManyToOne(targetEntity = User.class, optional = false)
    @JoinColumn(name = "user1_id")
    private User user1;
    
    @Column(name = "deck1")
    private long deck1;
    
    @ManyToOne(targetEntity = User.class, optional = false)
    @JoinColumn(name = "user2_id")
    private User user2;
	
	@Column(name = "event_log", length = 65000)
	private String log;
    
    @Column(name = "deck2")
    private long deck2;
    
    @ManyToOne(targetEntity = User.class, optional = false)
    @JoinColumn(name = "winner")
    private User userWin;
    
    @Column(name = "create_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Calendar creatingDate;
    
    @Column(name = "end_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Calendar endDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }

    public User getUserWin() {
        return userWin;
    }

    public void setUserWin(User userWin) {
        this.userWin = userWin;
    }

    public Calendar getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Calendar creatingDate) {
        this.creatingDate = creatingDate;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public long getDeck1() {
        return deck1;
    }

    public void setDeck1(long deck1) {
        this.deck1 = deck1;
    }

    public long getDeck2() {
        return deck2;
    }

    public void setDeck2(long deck2) {
        this.deck2 = deck2;
    }

 

    
    public BattleLog() {
    }

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}
	
	
    
    
}
