/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.shared;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author potapov
 */
public class Card implements Serializable {

	private int owner;

	private int position;

	private long id;

	private String name;

	private String descr;

	private int cost;

	private int health;

	private int currentHealth;

	private int damage;

	private ECardClass type;

	private String img;

	private String prev;

	private Speciality speciality;

	private SpecialEffect onActiv;

	private SpecialEffect onTurn;

	private SpecialEffect onDeath;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public SpecialEffect getOnActiv() {
		return onActiv;
	}

	public void setOnActiv(SpecialEffect onActiv) {
		this.onActiv = onActiv;
	}

	public SpecialEffect getOnTurn() {
		return onTurn;
	}

	public void setOnTurn(SpecialEffect onTurn) {
		this.onTurn = onTurn;
	}

	public SpecialEffect getOnDeath() {
		return onDeath;
	}

	public void setOnDeath(SpecialEffect onDeath) {
		this.onDeath = onDeath;
	}

	public Speciality getSpeciality() {
		return speciality;
	}

	public void setSpeciality(Speciality speciality) {
		this.speciality = speciality;
	}

	public Card() {
	}

	public ECardClass getType() {
		return type;
	}

	public void setType(String type) {
		this.type = ECardClass.valueOf(type);
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getPrev() {
		return prev;
	}

	public void setPrev(String prev) {
		this.prev = prev;
	}

	public int getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(int currentHealth) {
		this.currentHealth = currentHealth;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 47 * hash + this.owner;
		hash = 47 * hash + this.position;
		hash = 47 * hash + Objects.hashCode(this.name);
		hash = 47 * hash + this.cost;
		hash = 47 * hash + this.health;
		hash = 47 * hash + this.currentHealth;
		hash = 47 * hash + this.damage;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Card other = (Card) obj;
		if (this.owner != other.owner) {
			return false;
		}
		if (this.position != other.position) {
			return false;
		}
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		if (this.cost != other.cost) {
			return false;
		}
		if (this.health != other.health) {
			return false;
		}
		if (this.currentHealth != other.currentHealth) {
			return false;
		}
		if (this.damage != other.damage) {
			return false;
		}
		return true;
	}
}
