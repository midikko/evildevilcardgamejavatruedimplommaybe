/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import ru.group.thegame.dao.UserDAO;
import ru.group.thegame.domain.User;

/**
 *
 * @author potapov
 */
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDAO userDao;
	
	@Autowired
	private SpecialityService specialityService;
	
	@Override
	public User register(User entity) {
		return userDao.create(entity);
	}
	
	@Override
	public List<User> getAll() {
		return userDao.findAll();
	}
	
	@Override
	public User getById(long id) {
		return userDao.findByID(id);
	}
	
	@Override
	public void delete(User entity) {
		userDao.delete(entity);
	}
	
	@Override
	public User save(User entity) {
		return userDao.save(entity);
	}
	
	@Override
	public ru.group.thegame.shared.User init(User entity) {
		ru.group.thegame.shared.User shar = new ru.group.thegame.shared.User();
		shar.setCreatingDate(entity.getCreatingDate().getTime());
		shar.setId(entity.getId());
		shar.setLogin(entity.getLogin());
		shar.setName(entity.getName());
		shar.setPassword(entity.getPassword());
		shar.setEmail(entity.getEmail());
		shar.setRank(entity.getRank());
		shar.setRole(entity.getRole());
		shar.setAvatarUrl(entity.getAvatarUrl());
		shar.setSpeciality(specialityService.fromDomainToShared(entity.getSpeciality()));
		return shar;
	}
	
	@Override
	public User unwrap(ru.group.thegame.shared.User entity) {
		User domain = new User();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(entity.getCreatingDate());
		domain.setCreatingDate(calendar);
		domain.setId(entity.getId());
		domain.setLogin(entity.getLogin());
		domain.setName(entity.getName());
		domain.setPassword(entity.getPassword());
		domain.setEmail(entity.getEmail());
		domain.setRank(entity.getRank());
		domain.setRole(entity.getRole());
		domain.setAvatarUrl(entity.getAvatarUrl());
		domain.setSpeciality(specialityService.fromSharedToDomain(entity.getSpeciality()));
		return domain;
	}
	
	@Override
	public User findByLogin(String login) {
		return userDao.findByLogin(login);
	}
}
