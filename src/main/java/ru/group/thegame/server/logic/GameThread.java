/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.server.logic;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author potapov
 */
@Component
@Scope(value = "prototype")
@Configurable(preConstruction = true)
public class GameThread extends Thread{

    public GameThread(Runnable target) {
        super(target);
    }
    
}
