/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import ru.group.thegame.dao.CardDAO;
import ru.group.thegame.domain.Card;
import ru.group.thegame.domain.Speciality;
import ru.group.thegame.server.logic.Board;
import ru.group.thegame.server.logic.BoardManager;

/**
 *
 * @author potapov
 */
public class CardServiceImpl implements CardService {

	@Autowired
	CardDAO cardDao;

	@Autowired
	private SpecialEffectService specialEffectService;

	@Autowired
	private SpecialityService specialityService;

	@Override
	public void register(Card entity) {
		cardDao.create(entity);
	}

	@Override
	public List<Card> getAll() {
		return cardDao.findAll();
	}

	@Override
	public List<Card> getCommon() {
		return cardDao.findCommon();
	}

	@Override
	public List<Card> getBySpeciality(Speciality speciality) {
		return cardDao.findBySpeciality(speciality);
	}

	@Override
	public Card getById(long id) {
		return cardDao.findByID(id);
	}

	@Override
	public void delete(Card entity) {
		cardDao.delete(entity);
	}

	@Override
	public void save(Card entity) {
		cardDao.save(entity);
	}

	@Override
	public ru.group.thegame.shared.Card fromDomainToShared(Card entity) {
		ru.group.thegame.shared.Card shar = new ru.group.thegame.shared.Card();
		shar.setCost(entity.getCost());
		shar.setDamage(entity.getDamage());
		shar.setDescr(entity.getDescr());
		shar.setHealth(entity.getHealth());
		shar.setCurrentHealth(entity.getHealth());
		shar.setId(entity.getId());
		shar.setName(entity.getName());
		shar.setType(entity.getType().name());
		shar.setImg(entity.getImg());
		shar.setPrev(entity.getPrev());
		shar.setOnActiv(specialEffectService.fromDomainToShared(entity.getOnActiv()));
		shar.setOnDeath(specialEffectService.fromDomainToShared(entity.getOnDeath()));
		shar.setOnTurn(specialEffectService.fromDomainToShared(entity.getOnTurn()));
		shar.setSpeciality(specialityService.fromDomainToShared(entity.getSpeciality()));
		return shar;
	}

	@Override
	public Card fromSharedToDomain(ru.group.thegame.shared.Card entity) {
		Card domain = new Card();
		domain.setCost(entity.getCost());
		domain.setDamage(entity.getDamage());
		domain.setDescr(entity.getDescr());
		domain.setHealth(entity.getHealth());
		domain.setId(entity.getId());
		domain.setName(entity.getName());
		domain.setType(entity.getType().name());
		domain.setImg(entity.getImg());
		domain.setPrev(entity.getPrev());
		domain.setOnActiv(specialEffectService.fromSharedToDomain(entity.getOnActiv()));
		domain.setOnDeath(specialEffectService.fromSharedToDomain(entity.getOnDeath()));
		domain.setOnTurn(specialEffectService.fromSharedToDomain(entity.getOnTurn()));
		domain.setSpeciality(specialityService.fromSharedToDomain(entity.getSpeciality()));
		return domain;
	}

	@Override
	public ru.group.thegame.server.logic.Card fromDomainToLogic(Card entity) {
		ru.group.thegame.server.logic.Card logic = new ru.group.thegame.server.logic.Card();
		logic.setCost(entity.getCost());
		logic.setDamage(entity.getDamage());
		logic.setDescr(entity.getDescr());
		logic.setHealth(entity.getHealth());
		logic.getCardState().setCurrentHealth(entity.getHealth());
		logic.setName(entity.getName());
		logic.setType(ru.group.thegame.server.logic.ECardClass.valueOf(entity.getType().name()));
		logic.setImg(entity.getImg());
		logic.setPrev(entity.getPrev());
		logic.setOnActiv(specialEffectService.fromDomainToLogic(entity.getOnActiv()));
		logic.setOnDeath(specialEffectService.fromDomainToLogic(entity.getOnDeath()));
		logic.setOnTurn(specialEffectService.fromDomainToLogic(entity.getOnTurn()));
		logic.setSpeciality(specialityService.fromDomainToLogic(entity.getSpeciality()));
		return logic;
	}

	@Override
	public ru.group.thegame.server.logic.Card fromSharedToLogic(ru.group.thegame.shared.Card entity) {
		ru.group.thegame.server.logic.Card logic = new ru.group.thegame.server.logic.Card();
		logic.setCost(entity.getCost());
		logic.setDamage(entity.getDamage());
		logic.setDescr(entity.getDescr());
		logic.setHealth(entity.getHealth());
		logic.setOwner(entity.getOwner());
		logic.getCardState().setPosition(entity.getPosition());
		logic.getCardState().setCurrentHealth(entity.getCurrentHealth());
		logic.setName(entity.getName());
		logic.setType(ru.group.thegame.server.logic.ECardClass.valueOf(entity.getType().name()));
		logic.setImg(entity.getImg());
		logic.setPrev(entity.getPrev());
		logic.setOnActiv(specialEffectService.fromSharedToLogic(entity.getOnActiv()));
		logic.setOnDeath(specialEffectService.fromSharedToLogic(entity.getOnDeath()));
		logic.setOnTurn(specialEffectService.fromSharedToLogic(entity.getOnTurn()));
		logic.setSpeciality(specialityService.fromSharedToLogic(entity.getSpeciality()));
		return logic;
	}

	@Override
	public ru.group.thegame.shared.Card fromLogicToShared(ru.group.thegame.server.logic.Card card) {
		ru.group.thegame.shared.Card shar = new ru.group.thegame.shared.Card();
		shar.setCost(card.getCost());
		shar.setDamage(card.getDamage());
		shar.setDescr(card.getDescr());
		shar.setHealth(card.getHealth());
		shar.setOwner(card.getOwner());
		shar.setPosition(card.getCardState().getPosition());
		shar.setCurrentHealth(card.getCardState().getCurrentHealth());
		shar.setName(card.getName());
		shar.setType(card.getType().name());
		shar.setImg(card.getImg());
		shar.setPrev(card.getPrev());
		shar.setOnActiv(specialEffectService.fromLogicToShared(card.getOnActiv()));
		shar.setOnDeath(specialEffectService.fromLogicToShared(card.getOnDeath()));
		shar.setOnTurn(specialEffectService.fromLogicToShared(card.getOnTurn()));
		shar.setSpeciality(specialityService.fromLogicToShared(card.getSpeciality()));
		return shar;
	}
}
