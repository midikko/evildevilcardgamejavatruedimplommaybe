/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic.event;

import java.io.Serializable;
import ru.group.thegame.server.logic.state.CardState;

/**
 *
 * @author nevezhinpavel
 */
public class CardAdditionEvent implements Event, Serializable{
	
	private int owner;
	
	private CardState card;	

	public CardAdditionEvent(int owner, CardState card) {
		this.owner = owner;
		this.card = card;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public CardState getCard() {
		return card;
	}

	public void setCard(CardState card) {
		this.card = card;
	}
}
