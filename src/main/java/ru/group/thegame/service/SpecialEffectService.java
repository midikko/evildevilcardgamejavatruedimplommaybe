/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import java.util.List;
import ru.group.thegame.domain.SpecialEffect;

/**
 *
 * @author potapov
 */
public interface SpecialEffectService {
    
    public void register(SpecialEffect entity);

    public List<SpecialEffect> getAll();

    public SpecialEffect getById(long id);

    public void delete(SpecialEffect entity);

    public void save(SpecialEffect entity);
    
    public ru.group.thegame.shared.SpecialEffect fromDomainToShared(SpecialEffect enity);

    public SpecialEffect fromSharedToDomain(ru.group.thegame.shared.SpecialEffect entity);
    
    public ru.group.thegame.server.logic.SpecialEffect fromDomainToLogic(SpecialEffect enity);

    public  ru.group.thegame.server.logic.SpecialEffect fromSharedToLogic(ru.group.thegame.shared.SpecialEffect entity);

	public ru.group.thegame.shared.SpecialEffect fromLogicToShared(ru.group.thegame.server.logic.SpecialEffect onActiv);
}
