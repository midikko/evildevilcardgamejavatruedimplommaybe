/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.domain;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nikolaev
 */
@Entity
@Table(name = "user")
public class User implements Serializable {

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "user_role", length = 255, nullable = false)
    private long role;

    @Column(name = "login", length = 255, nullable = false, unique = true)
    private String login;

    @Column(name = "password", length = 255, nullable = false)
    private String password;

    @Column(name = "name", length = 255)
    private String name;
    
    @Column(name = "avatar_url", length = 255)
    private String avatarUrl = "img/ava-2.png";
    
    @Column(name = "e-mail", length = 255)
    private String email;
    
    @Column(name = "rank")
    private long rank;
    
    @ManyToOne(targetEntity = Speciality.class, optional = false)
    @JoinColumn(name = "speciality_id")
    private Speciality speciality;

    @Column(name = "create_date")
    @Temporal(value = TemporalType.TIMESTAMP)
    private Calendar creatingDate;
    
    @OneToMany(targetEntity = BattleLog.class, cascade = CascadeType.ALL)
    private Set battles;

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public User() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getRank() {
        return rank;
    }

    public void setRank(long rank) {
        this.rank = rank;
    }

    public User(long id, String login, String password, String name, Calendar creatingDate) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.name = name;
        this.creatingDate = creatingDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getCreatingDate() {
        return creatingDate;
    }

    public void setCreatingDate(Calendar creatingDate) {
        this.creatingDate = creatingDate;
    }

    public long getRole() {
        return role;
    }

    public void setRole(long role) {
        this.role = role;
    }

	public Set getBattles() {
		return battles;
	}

	public void setBattles(Set battles) {
		this.battles = battles;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 53 * hash + (int) (this.id ^ (this.id >>> 32));
		hash = 53 * hash + (int) (this.role ^ (this.role >>> 32));
		hash = 53 * hash + Objects.hashCode(this.login);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final User other = (User) obj;
		if (this.id != other.id) {
			return false;
		}
		if (!Objects.equals(this.login, other.login)) {
			return false;
		}
		return true;
	}

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
    
    
}
