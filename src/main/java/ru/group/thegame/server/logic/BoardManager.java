/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import ru.group.thegame.Initiator;

/**
 *
 * @author Nevezhin_Pavel
 */
public class BoardManager {

	private final Board board;

	public BoardManager(Board board) {
		this.board = board;
	}

	public Board getBoard() {
		return board;
	}

	public void applySpecialEffect(Card card, SpecialEffect specialEffect) {

		List<Card> userCards = board.getUserCards(card.getOwner());

		List<Card> enemyCards = board.getUserCards(card.getOwner() == 0 ? 1 : 0);

		String method = specialEffect.getMethod();
		String type = specialEffect.getType();
		int power = specialEffect.getPower();

		if (type.equalsIgnoreCase("damage")) {
			power *= -1;
		}

		if (Initiator.DEBUG) {
			System.out.println("\nIn " + this.getClass().getName() + "  ::  method=" + method + "\n");
			System.out.println("\nIn " + this.getClass().getName() + "  ::  type=" + type + "\n");
			System.out.println("\nIn " + this.getClass().getName() + "  ::  power=" + power + "\n");
			System.out.println("\nIn " + this.getClass().getName() + "  ::  name=" + card.getName() + "  ::  hp=" + card.getCardState().getCurrentHealth() + "\n");
		}
		switch (method) {
			case "all":
				for (Card currentCard : userCards) {
					action(currentCard, power);
				}
				for (Card currentCard : enemyCards) {
					action(currentCard, power);
				}
				break;
			case "all_enemy":
				for (Card currentCard : enemyCards) {
					action(currentCard, power);
				}
				break;
			case "all_alias":
				for (Card currentCard : userCards) {
					action(currentCard, power);
				}
				break;
			case "in_front":
				Card currentCard = enemyCards.get(card.getCardState().getPosition());
				action(currentCard, power);
				break;
			case "self":
				action(card, power);
				break;
			case "near":
				for (int i = card.getCardState().getPosition() - 1; i < card.getCardState().getPosition() + 2; i++) {
					currentCard = userCards.get(i);
					action(currentCard, power);
				}
				break;
			case "random_all":
				List<Card> list = ThreadLocalRandom.current().nextInt(0, 2) == 0 ? userCards : enemyCards;
				int cardIndex = ThreadLocalRandom.current().nextInt(0, 6);
				while (list.get(cardIndex) == null) {
					cardIndex = ThreadLocalRandom.current().nextInt(0, 6);
				}
				currentCard = list.get(cardIndex);
				action(currentCard, power);
				break;
			case "random_enemy":
				list = enemyCards;
				cardIndex = ThreadLocalRandom.current().nextInt(0, 6);
				while (list.get(cardIndex) == null) {
					cardIndex = ThreadLocalRandom.current().nextInt(0, 6);
				}
				currentCard = list.get(cardIndex);
				action(currentCard, power);
				break;
			case "random_alias":
				list = userCards;
				cardIndex = ThreadLocalRandom.current().nextInt(0, 6);
				while (list.get(cardIndex) == null) {
					cardIndex = ThreadLocalRandom.current().nextInt(0, 6);
				}
				currentCard = list.get(cardIndex);
				action(currentCard, power);
				break;
		}
	}

	public boolean putCard(Card card) {
		return board.putCard(card);
	}

	boolean removeCard(Card card) {
		return board.removeCard(card.getOwner(), card.getCardState().getPosition());
	}

	private void action(Card currentCard, int power) {
		if (currentCard != null) {
			if ((currentCard.getCardState().getCurrentHealth() + power) > currentCard.getHealth()) {
				currentCard.getCardState().setCurrentHealth(currentCard.getHealth());
			} else if ((currentCard.getCardState().getCurrentHealth() + power) < 1) {
				if (Initiator.DEBUG) {
					System.out.println("\nIn  ::  name=" + currentCard.getName() + " is removed  ::  hp=" + (currentCard.getCardState().getCurrentHealth() + power) + "\n");
				}
				currentCard.removeCard();
			} else {
				currentCard.getCardState().setCurrentHealth(currentCard.getCardState().getCurrentHealth() + power);
			}
		}
	}
}
