/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic.event;

import java.io.Serializable;

/**
 *
 * @author nevezhinpavel
 */
public class CardDeathEvent implements Event, Serializable {
	
	private int owner;
	
	private int position;

	public CardDeathEvent(int owner, int position) {
		this.owner = owner;
		this.position = position;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
}
