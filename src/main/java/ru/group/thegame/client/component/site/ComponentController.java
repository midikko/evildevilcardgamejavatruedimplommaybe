/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.component.site;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasWidgets;
import ru.group.thegame.client.Presenter;
import ru.group.thegame.client.component.Component;
import ru.group.thegame.client.component.site.presenter.HomePresenter;
import ru.group.thegame.client.component.site.view.HomeView;

/**
 *
 * @author MasterKrab
 */
public class ComponentController implements Component, ValueChangeHandler<String> {

	private HasWidgets container;
	private final HandlerManager eventBus;

	public ComponentController(HandlerManager eventBus) {
		this.eventBus = eventBus;
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		this.container = container;

		openPage(getDefaultHistoryToken());
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		openPage(event.getValue());
	}

	private void openPage(String token) {
		if (token != null) {
			Presenter presenter = null;
			String[] split = token.split("-");
			if (split[0].equals("site")) {
				switch (split[1]) {
					case "about":
						break;
					case "home":
						presenter = new HomePresenter(eventBus, new HomeView());
						break;
				}
				if (presenter != null) {
					presenter.go(container);
				}
			}
		}
	}

	@Override
	public String getDefaultHistoryToken() {
		return "site-home";
	}
}
