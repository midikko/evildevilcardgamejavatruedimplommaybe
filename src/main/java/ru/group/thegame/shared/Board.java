/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Nevezhin_Pavel
 */
public class Board implements Serializable {

	private List<Card> user1Cards = new ArrayList<>(6);

	private List<Card> user2Cards = new ArrayList<>(6);

	public Board() {
	}

	public List<Card> getUser1Cards() {
		return user1Cards;
	}

	public void setUser1Cards(List<Card> user1Cards) {
		this.user1Cards = user1Cards;
	}

	public List<Card> getUser2Cards() {
		return user2Cards;
	}

	public void setUser2Cards(List<Card> user2Cards) {
		this.user2Cards = user2Cards;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + Objects.hashCode(this.user1Cards);
		hash = 53 * hash + Objects.hashCode(this.user2Cards);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Board other = (Board) obj;
		if (!Objects.equals(this.user1Cards, other.user1Cards)) {
			return false;
		}
		if (!Objects.equals(this.user2Cards, other.user2Cards)) {
			return false;
		}
		return true;
	}
}
