/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.server.rpc;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import ru.group.thegame.client.PrivateRPCService;

/**
 *
 * @author Nevezhin_Pavel
 */
@Service("PrivateRPCService")
public class PrivateRPCServiceImpl implements PrivateRPCService{

	@Override
	public boolean isAuthenticated() {
		return SecurityContextHolder.getContext().getAuthentication().isAuthenticated();
	}
	
	@Override
	public boolean isRoot(){
		User u = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		return u.getUsername().equals("root");
	}
	
}
