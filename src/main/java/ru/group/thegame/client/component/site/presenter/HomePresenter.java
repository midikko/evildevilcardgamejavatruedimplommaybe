/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.client.component.site.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import ru.group.thegame.client.Presenter;

/**
 *
 * @author Nevezhin_Pavel
 */
public class HomePresenter implements Presenter {

	public interface Display {

		public Widget asWidget();
	}
	
	private final Display display;
	private final HandlerManager eventBus;

	public HomePresenter(HandlerManager eventBus, Display display) {
		this.display = display;
		this.eventBus = eventBus;
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());
	}
}
