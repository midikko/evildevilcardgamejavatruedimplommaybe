/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic;

import java.util.Objects;
import ru.group.thegame.Initiator;
import ru.group.thegame.server.logic.state.CardState;

/**
 *
 *
 */
public class Card implements Comparable<Card> {

	private int owner;

	private BoardManager boardManager;

	private String name;

	private String descr;

	private String img;

	private String prev;

	private Speciality speciality;

	private ECardClass type;

	private int health;

	private int cost;

	private int damage;

	private SpecialEffect onActiv;

	private SpecialEffect onTurn;

	private SpecialEffect onDeath;

	private final CardState cardState;

	public Card() {
		cardState = new CardState();
	}

	public CardState getCardState() {
		return cardState;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public BoardManager getBoardManager() {
		return boardManager;
	}

	public void setBoardManager(BoardManager boardManager) {
		this.boardManager = boardManager;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getPrev() {
		return prev;
	}

	public void setPrev(String prev) {
		this.prev = prev;
	}

	public Speciality getSpeciality() {
		return speciality;
	}

	public void setSpeciality(Speciality speciality) {
		this.speciality = speciality;
	}

	public ECardClass getType() {
		return type;
	}

	public void setType(ECardClass type) {
		this.type = type;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public SpecialEffect getOnActiv() {
		return onActiv;
	}

	public void setOnActiv(SpecialEffect onActiv) {
		this.onActiv = onActiv;
	}

	public SpecialEffect getOnTurn() {
		return onTurn;
	}

	public void setOnTurn(SpecialEffect onTurn) {
		this.onTurn = onTurn;
	}

	public SpecialEffect getOnDeath() {
		return onDeath;
	}

	public void setOnDeath(SpecialEffect onDeath) {
		this.onDeath = onDeath;
	}

	public void onTurn() {
		boardManager.applySpecialEffect(this, onTurn);
	}

	public void onActivation() {
		boardManager.applySpecialEffect(this, onActiv);
	}

	public void onDeath() {
		boardManager.applySpecialEffect(this, onDeath);
	}

	public boolean putCard(int position) {
		if (Initiator.DEBUG) {
			System.out.println("\n\nDEBUG Board when put card  IN" + this.getClass().getName() + "::" + boardManager.getBoard() + "\n");
		}
		this.cardState.setPosition(position);
		onActivation();
		boolean putCard = boardManager.putCard(this);
		return putCard;
	}

	public boolean removeCard() {
		if (Initiator.DEBUG) {
			System.out.println("\n\nDEBUG Board when remove card  IN" + this.getClass().getName() + "::" + boardManager.getBoard() + "\n");
		}
		onDeath();
		boolean removeCard = boardManager.removeCard(this);
		return removeCard;
	}

	@Override
	public int compareTo(Card o) {
		int i = this.speciality.compareTo(o.getSpeciality());
		return (i == 0) ? this.name.compareTo(o.getName()) : i;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 97 * hash + this.cost;
		hash = 97 * hash + Objects.hashCode(this.speciality);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Card other = (Card) obj;
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		if (this.cost != other.cost) {
			return false;
		}
		if (!Objects.equals(this.speciality, other.speciality)) {
			return false;
		}
		return true;
	}
}
