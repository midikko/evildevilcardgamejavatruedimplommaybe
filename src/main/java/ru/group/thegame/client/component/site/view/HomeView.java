/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.component.site.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import ru.group.thegame.client.component.site.presenter.HomePresenter;

/**
 *
 * @author Nevezhin_Pavel
 */
public class HomeView extends Composite implements HomePresenter.Display {

	FlowPanel flowPanel = new FlowPanel();
		
	public HomeView() {
		initWidget(flowPanel);
		final PopupPanel box = new PopupPanel(true, true);
		box.add(new Label("new label"));
		final Label l = new Label("Name ability");
		final PopupPanel panel = new PopupPanel(true, true);
		final FlowPanel fp = new FlowPanel();
		fp.setHeight("200");
		flowPanel.add(fp);
		l.addMouseOverHandler(new MouseOverHandler() {

			@Override
			public void onMouseOver(MouseOverEvent event) {
				fp.clear();
				l.setText(event.getX() + "   =    " + event.getY());
				fp.add(new Label("Description"));
				fp.setVisible(true);
			}
		});
		l.addMouseOutHandler(new MouseOutHandler() {

			@Override
			public void onMouseOut(MouseOutEvent event) {
				fp.setVisible(false);
			}
		});
		flowPanel.add(l);
		flowPanel.add(new Button("click", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				box.show();
			}
		}));
	}

	@Override
	public Widget asWidget() {
		return this;
	}
}
