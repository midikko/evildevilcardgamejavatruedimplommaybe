/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.client.event;

import com.google.gwt.event.shared.GwtEvent;
import java.util.Map;

/**
 *
 * @author nikolaev
 */
public class OpenPageEvent extends GwtEvent<OpenPageEventHandler>{

	public static Type<OpenPageEventHandler> TYPE = new Type<OpenPageEventHandler>();
	public Class target;
	public Map<String, String> parameters;

	public OpenPageEvent(Class target) {
		super();
		this.target = target;
	}
	
	public OpenPageEvent setParameters( Map<String, String> parameters ){
		this.parameters = parameters;
		return this;
	}
	
	@Override
	public Type<OpenPageEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(OpenPageEventHandler handler) {
		handler.onOpenPage( this );
	}
	
}
