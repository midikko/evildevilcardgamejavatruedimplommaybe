/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.dao;

import java.util.List;
import ru.group.thegame.domain.User;

/**
 *
 * @author potapov
 */
public interface UserDAO {

	public User create(User entity);

	public User save(User entity);

	public void delete(User entity);

	public List findAll();

	public User findByID(long id);

	public User findByLogin(String login);

	public boolean isExist(User entity);
}
