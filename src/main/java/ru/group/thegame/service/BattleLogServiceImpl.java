/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import ru.group.thegame.dao.BattleLogDAO;
import ru.group.thegame.domain.BattleLog;

/**
 *
 * @author potapov
 */
public class BattleLogServiceImpl implements BattleLogService{

    @Autowired
    private BattleLogDAO battleLogDao;
    
    @Override
    public void register(BattleLog entity) {
        battleLogDao.create(entity);
    }

    @Override
    public List<BattleLog> getAll() {
        return battleLogDao.findAll();
    }

    @Override
    public BattleLog getById(long id) {
        return battleLogDao.findByID(id);
    }

    @Override
    public void delete(BattleLog entity) {
        battleLogDao.delete(entity);
    }

    @Override
    public void save(BattleLog entity) {
        battleLogDao.save(entity);
    }
    
}
