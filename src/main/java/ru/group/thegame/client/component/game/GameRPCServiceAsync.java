/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.component.game;

import com.google.gwt.user.client.rpc.AsyncCallback;
import java.util.List;
import ru.group.thegame.shared.Card;
import ru.group.thegame.shared.User;
import ru.group.thegame.shared.Game;

/**
 *
 * @author MasterKrab
 */
public interface GameRPCServiceAsync {

	public void getOnlineUsers(AsyncCallback<List<User>> callback);

	public void startGame(AsyncCallback<Void> callback);

	public void leaveQueue(AsyncCallback<Void> callback);
	
	public void getCommonCards(AsyncCallback<List<Card>> callback);
	
	public void amIPlaying(AsyncCallback<Long> callback);
	
	public void getGameByUID(long uid, AsyncCallback<Game> callback);
	
	public void surrender(long id, AsyncCallback<Void> callback);

	public void getCurrentUser(AsyncCallback<User> asyncCallback);

	public void putCard(Card selected, int j, AsyncCallback<Void> asyncCallback);

	public void skipTurn(AsyncCallback<Void> callback);
}
