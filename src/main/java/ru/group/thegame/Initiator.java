/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame;

import java.util.Calendar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.group.thegame.domain.BattleLog;
import ru.group.thegame.domain.Card;
import ru.group.thegame.domain.ECardClass;
import ru.group.thegame.domain.SpecialEffect;
import ru.group.thegame.domain.Speciality;
import ru.group.thegame.domain.User;
import ru.group.thegame.server.logic.GameManager;
import ru.group.thegame.service.BattleLogService;
import ru.group.thegame.service.CardService;
import ru.group.thegame.service.SpecialEffectService;
import ru.group.thegame.service.SpecialityService;
import ru.group.thegame.service.UserService;

/**
 *
 * @author potapov
 */
public class Initiator {

	public static final boolean DEBUG = true;
	public static final boolean DEEP_DEBUG = true;
	public static final boolean STRESS_TEST = false;
	public static final int STRESS_TEST_USER_COUNT = 1000;

	@Autowired
	private SpecialEffectService specialEffectService;
	@Autowired
	private SpecialityService specialityService;
	@Autowired
	private CardService cardService;
	@Autowired
	private UserService userService;
	@Autowired
	private BattleLogService battleLogService;
	@Autowired
	private GameManager GM;
	@Autowired
	private ApplicationContext appContext;

	public void init() {
		
		if (DEEP_DEBUG) {
		System.out.println("\n====================\n" + "INICIATION IS ABOUT TO BEGIN" + "\n====================\n");
		
			System.out.println("===============# BEAN CONFIG INFO #===============");
			System.out.println("specialEffectService :: " + specialEffectService);
			System.out.println("specialityService :: " + specialityService);
			System.out.println("cardService :: " + cardService);
			System.out.println("userService :: " + userService);
			System.out.println("battleLogService :: " + battleLogService);
			System.out.println("GM :: " + GM);
			System.out.println("appContext :: " + appContext);
		}

		if (DEBUG) {
			System.out.println("\n=================\n" + "## : SE1 CREATED" + "\n=================\n");
		}
		SpecialEffect se = new SpecialEffect();
		se.setDescr("Damage 3");
		se.setMethod("all");
		se.setName("damage 3");
		se.setPower(3);
		se.setType("damage");
		specialEffectService.register(se);

		if (DEBUG) {
			System.out.println("\n=================\n" + "## : SE2 CREATED" + "\n=================\n");
		}
		SpecialEffect se1 = new SpecialEffect();
		se1.setDescr("damage");
		se1.setMethod("all");
		se1.setName("heal 4");
		se1.setPower(4);
		se1.setType("heal");
		specialEffectService.register(se1);

		if (DEBUG) {
			System.out.println("\n=================\n" + "## : SE3 CREATED" + "\n=================\n");
		}
		SpecialEffect se2 = new SpecialEffect();
		se2.setDescr("Slow");
		se2.setMethod("all");
		se2.setName("heal 0");
		se2.setPower(0);
		se2.setType("heal");
		specialEffectService.register(se2);
		Speciality[] spec = new Speciality[5];

		spec[0] = new Speciality();
		spec[0].setName("Fire");
		spec[0].setDescr("MUCH FUIR");
		specialityService.register(spec[0]);

		if (DEBUG) {
			System.out.println("\n=================\n" + "## : SPECIALITY{" + spec[0].getName() + "] CREATED" + "\n=================\n");
		}

		spec[1] = new Speciality();
		spec[1].setName("Water");
		spec[1].setDescr("SUCH WATER");
		specialityService.register(spec[1]);

		if (DEBUG) {
			System.out.println("\n=================\n" + "## : SPECIALITY{" + spec[1].getName() + "] CREATED" + "\n=================\n");
		}

		spec[2] = new Speciality();
		spec[2].setName("Air");
		spec[2].setDescr("F-E-E-E-W F-E-E-E-W MOTHER FUCKER");
		specialityService.register(spec[2]);

		if (DEBUG) {
			System.out.println("\n=================\n" + "## : SPECIALITY{" + spec[2].getName() + "] CREATED" + "\n=================\n");
		}

		spec[3] = new Speciality();
		spec[3].setName("Earth");
		spec[3].setDescr("DO YOU WANNA BE FACE-HITTED BY THE STONE, BADASS?!?");
		specialityService.register(spec[3]);

		if (DEBUG) {
			System.out.println("\n=================\n" + "## : SPECIALITY{" + spec[3].getName() + "] CREATED" + "\n=================\n");
		}

		spec[4] = new Speciality();
		spec[4].setName("Naruto");
		spec[4].setDescr("WTF !? NARUTO CARDS!?!?!?!? ");
		specialityService.register(spec[4]);

		if (DEBUG) {
			System.out.println("\n=================\n" + "## : SPECIALITY{" + spec[4].getName() + "] CREATED" + "\n=================\n");
		}

		for (int j = 0; j < 4; j++) {
			Card[] cards = new Card[12];
			for (int i = 0; i < 12; i++) {
				cards[i] = new Card();
				cards[i].setCost(i);
				cards[i].setName("FUIR CARD NUMBORE " + i);
				cards[i].setDamage(i);
				if (i < 5) {
					cards[i].setDescr("Less powerful");
				} else {
					cards[i].setDescr("Much powerful");
				}
				cards[i].setHealth(i);
				cards[i].setOnActiv(se);
				cards[i].setOnDeath(se1);
				cards[i].setOnTurn(se2);
				cards[i].setSpeciality(spec[j]);
				cards[i].setType(ECardClass.MOB.name());
				cardService.register(cards[i]);
				if (DEBUG) {
					System.out.println("\n=================\n" + "## : CARD{" + i + "] CREATED" + "\n=================\n");
				}
			}
//			cards[11] = new Card();
//			cards[11].setCost(3);
//			cards[11].setDescr("SPELLO DAMAGIO");
//			cards[11].setName("FIRSTO SPELLO 1");
//			cards[11].setOnActiv(se);
//			cards[11].setSpeciality(spec[j]);
//			cards[11].setType(ECardClass.SPELL.name());

		}

		Card[] cards = new Card[12];

		cards[0] = new Card();
		cards[0].setImg("img/card/naruto/full/card-1.png");
		cards[0].setPrev("img/card/naruto/prev/card-1.png");
		cards[0].setCost(2);
		cards[0].setName("Naruto");
		cards[0].setDamage(3);
		if (0 < 5) {
			cards[0].setDescr("Less powerful");
		} else {
			cards[0].setDescr("Much powerful");
		}
		cards[0].setHealth(24);
		cards[0].setOnActiv(se);
		cards[0].setOnDeath(se1);
		cards[0].setOnTurn(se2);
		cards[0].setSpeciality(spec[4]);
		cards[0].setType(ECardClass.MOB.name());
		cardService.register(cards[0]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <NARUTO> CREATED" + "\n=================\n");
		}

		cards[1] = new Card();
		cards[1].setImg("img/card/naruto/full/card-2.png");
		cards[1].setPrev("img/card/naruto/prev/card-2.png");
		cards[1].setCost(3);
		cards[1].setName("Kakashi");
		cards[1].setDamage(5);
		if (0 < 5) {
			cards[1].setDescr("Less powerful");
		} else {
			cards[1].setDescr("Much powerful");
		}
		cards[1].setHealth(30);
		cards[1].setOnActiv(se);
		cards[1].setOnDeath(se1);
		cards[1].setOnTurn(se2);
		cards[1].setSpeciality(spec[4]);
		cards[1].setType(ECardClass.MOB.name());
		cardService.register(cards[1]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <KAKASHI> CREATED" + "\n=================\n");
		}

		cards[2] = new Card();
		cards[2].setImg("img/card/naruto/full/card-3.png");
		cards[2].setPrev("img/card/naruto/prev/card-3.png");
		cards[2].setCost(8);
		cards[2].setName("Minato");
		cards[2].setDamage(7);
		if (0 < 5) {
			cards[2].setDescr("Less powerful");
		} else {
			cards[2].setDescr("Much powerful");
		}
		cards[2].setHealth(48);
		cards[2].setOnActiv(se);
		cards[2].setOnDeath(se1);
		cards[2].setOnTurn(se2);
		cards[2].setSpeciality(spec[4]);
		cards[2].setType(ECardClass.MOB.name());
		cardService.register(cards[2]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <MINATO> CREATED" + "\n=================\n");
		}

		cards[3] = new Card();
		cards[3].setImg("img/card/naruto/full/card-4.png");
		cards[3].setPrev("img/card/naruto/prev/card-4.png");
		cards[3].setCost(1);
		cards[3].setName("Sacura");
		cards[3].setDamage(2);
		if (0 < 5) {
			cards[3].setDescr("Less powerful");
		} else {
			cards[3].setDescr("Much powerful");
		}
		cards[3].setHealth(50);
		cards[3].setOnActiv(se);
		cards[3].setOnDeath(se1);
		cards[3].setOnTurn(se2);
		cards[3].setSpeciality(spec[4]);
		cards[3].setType(ECardClass.MOB.name());
		cardService.register(cards[3]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <SAKURA> CREATED" + "\n=================\n");
		}

		cards[4] = new Card();
		cards[4].setImg("img/card/naruto/full/card-5.png");
		cards[4].setPrev("img/card/naruto/prev/card-5.png");
		cards[4].setCost(1);
		cards[4].setName("Sasuke");
		cards[4].setDamage(7);
		if (0 < 5) {
			cards[4].setDescr("Less powerful");
		} else {
			cards[4].setDescr("Much powerful");
		}
		cards[4].setHealth(35);
		cards[4].setOnActiv(se);
		cards[4].setOnDeath(se1);
		cards[4].setOnTurn(se2);
		cards[4].setSpeciality(spec[4]);
		cards[4].setType(ECardClass.MOB.name());
		cardService.register(cards[4]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <SASUKE> CREATED" + "\n=================\n");
		}

		cards[5] = new Card();
		cards[5].setImg("img/card/naruto/full/card-6.png");
		cards[5].setPrev("img/card/naruto/prev/card-6.png");
		cards[5].setCost(4);
		cards[5].setName("Ino");
		cards[5].setDamage(4);
		if (0 < 5) {
			cards[5].setDescr("Less powerful");
		} else {
			cards[5].setDescr("Much powerful");
		}
		cards[5].setHealth(25);
		cards[5].setOnActiv(se);
		cards[5].setOnDeath(se1);
		cards[5].setOnTurn(se2);
		cards[5].setSpeciality(spec[4]);
		cards[5].setType(ECardClass.MOB.name());
		cardService.register(cards[5]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <INO> CREATED" + "\n=================\n");
		}

		cards[6] = new Card();
		cards[6].setImg("img/card/naruto/full/card-7.png");
		cards[6].setPrev("img/card/naruto/prev/card-7.png");
		cards[6].setCost(12);
		cards[6].setName("Джирайя");
		cards[6].setDamage(10);
		if (0 < 5) {
			cards[6].setDescr("Less powerful");
		} else {
			cards[6].setDescr("Much powerful");
		}
		cards[6].setHealth(40);
		cards[6].setOnActiv(se);
		cards[6].setOnDeath(se1);
		cards[6].setOnTurn(se2);
		cards[6].setSpeciality(spec[4]);
		cards[6].setType(ECardClass.MOB.name());
		cardService.register(cards[6]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <Джирайя> CREATED" + "\n=================\n");
		}

		cards[7] = new Card();
		cards[7].setImg("img/card/naruto/full/card-8.png");
		cards[7].setPrev("img/card/naruto/prev/card-8.png");
		cards[7].setCost(4);
		cards[7].setName("Дейдара");
		cards[7].setDamage(5);
		if (0 < 5) {
			cards[7].setDescr("Less powerful");
		} else {
			cards[7].setDescr("Much powerful");
		}
		cards[7].setHealth(20);
		cards[7].setOnActiv(se);
		cards[7].setOnDeath(se1);
		cards[7].setOnTurn(se2);
		cards[7].setSpeciality(spec[4]);
		cards[7].setType(ECardClass.MOB.name());
		cardService.register(cards[7]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <Дейдара> CREATED" + "\n=================\n");
		}

		cards[8] = new Card();
		cards[8].setImg("img/card/naruto/full/card-9.png");
		cards[8].setPrev("img/card/naruto/prev/card-9.png");
		cards[8].setCost(10);
		cards[8].setName("Итачи");
		cards[8].setDamage(15);
		if (0 < 5) {
			cards[8].setDescr("Less powerful");
		} else {
			cards[8].setDescr("Much powerful");
		}
		cards[8].setHealth(40);
		cards[8].setOnActiv(se);
		cards[8].setOnDeath(se1);
		cards[8].setOnTurn(se2);
		cards[8].setSpeciality(spec[4]);
		cards[8].setType(ECardClass.MOB.name());
		cardService.register(cards[8]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <Итачи> CREATED" + "\n=================\n");
		}

		cards[9] = new Card();
		cards[9].setImg("img/card/naruto/full/card-10.png");
		cards[9].setPrev("img/card/naruto/prev/card-10.png");
		cards[9].setCost(6);
		cards[9].setName("Гаара");
		cards[9].setDamage(50);
		if (0 < 5) {
			cards[9].setDescr("Less powerful");
		} else {
			cards[9].setDescr("Much powerful");
		}
		cards[9].setHealth(2);
		cards[9].setOnActiv(se);
		cards[9].setOnDeath(se1);
		cards[9].setOnTurn(se2);
		cards[9].setSpeciality(spec[4]);
		cards[9].setType(ECardClass.MOB.name());
		cardService.register(cards[9]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <Гаара> CREATED" + "\n=================\n");
		}

		cards[10] = new Card();
		cards[10].setImg("img/card/naruto/full/card-11.png");
		cards[10].setPrev("img/card/naruto/prev/card-11.png");
		cards[10].setCost(8);
		cards[10].setName("Мадара");
		cards[10].setDamage(3);
		if (0 < 5) {
			cards[10].setDescr("Less powerful");
		} else {
			cards[10].setDescr("Much powerful");
		}
		cards[10].setHealth(100);
		cards[10].setOnActiv(se);
		cards[10].setOnDeath(se1);
		cards[10].setOnTurn(se2);
		cards[10].setSpeciality(spec[4]);
		cards[10].setType(ECardClass.MOB.name());
		cardService.register(cards[10]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <Мадара> CREATED" + "\n=================\n");
		}

		cards[11] = new Card();
		cards[11].setImg("img/card/naruto/full/card-12.png");
		cards[11].setPrev("img/card/naruto/prev/card-12.png");
		cards[11].setCost(1);
		cards[11].setName("SPELLO MAGICA");
		cards[11].setDamage(1);
		if (0 < 5) {
			cards[11].setDescr("Less powerful");
		} else {
			cards[11].setDescr("Much powerful");
		}
		cards[11].setHealth(20);
		cards[11].setOnActiv(se);
		cards[11].setOnDeath(se1);
		cards[11].setOnTurn(se2);
		cards[11].setSpeciality(spec[4]);
		cards[11].setType(ECardClass.MOB.name());
		cardService.register(cards[11]);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : CARD <Лох> CREATED" + "\n=================\n");
		}

		User user = new User();
		user.setLogin("midikko");
		user.setName("matrecs");
		user.setPassword("123qwe");
		user.setRole(2);
		user.setRank(1);
		user.setSpeciality(spec[0]);
		user.setCreatingDate(Calendar.getInstance());
		user.setEmail("matrecs@gmail.com");
		user.setAvatarUrl("img/ava-3.jpg");
		userService.register(user);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : USER{" + user.getName() + "} CREATED" + "\n=================\n");
		}

		User user1 = new User();
		user1.setLogin("addBoyNick");
		user1.setName("addBoyNick");
		user1.setPassword("qwe123");
		user1.setRole(2);
		user1.setRank(1);
		user1.setSpeciality(spec[1]);
		user1.setCreatingDate(Calendar.getInstance());
		user1.setEmail("pashkinapochta@gmail.com");
		userService.register(user1);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : USER{" + user1.getName() + "] CREATED" + "\n=================\n");
		}

		User user2 = new User();
		user2.setLogin("root");
		user2.setName("wolf");
		user2.setPassword("12");
		user2.setRole(2);
		user2.setRank(1);
		user2.setSpeciality(spec[4]);
		user2.setCreatingDate(Calendar.getInstance());
		user2.setEmail("white_wolf_17@mail.com");
		user2.setAvatarUrl("img/ava.jpg");
		userService.register(user2);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : USER{" + user2.getName() + "} CREATED" + "\n=================\n");
		}

		BattleLog log = new BattleLog();
		log.setCreatingDate(Calendar.getInstance());
		log.setUser1(user);
		log.setUser2(user1);
		log.setDeck1(user.getSpeciality().getId());
		log.setDeck2(user1.getSpeciality().getId());
		log.setUserWin(user);
		log.setEndDate(Calendar.getInstance());
		battleLogService.register(log);
		if (DEBUG) {
			System.out.println("\n=================\n" + "## : BattleLog between{" + user1.getName() + "] and {" + user.getName() + "} CREATED" + "\n=================\n");
		}

		if (STRESS_TEST) {
			for (int i = 0; i < STRESS_TEST_USER_COUNT; i++) {
				User userX = new User();
				userX.setLogin("testUserX" + i);
				userX.setName("testUserX" + i);
				userX.setPassword("qwe123" + i);
				userX.setRole(2);
				userX.setRank(1);
				userX.setSpeciality(spec[1]);
				userX.setCreatingDate(Calendar.getInstance());
				userX.setEmail("testUserX" + i + "@gmail.com");
				userService.register(userX);
				if (DEBUG) {
				System.out.println("\n=================\n" + "## : USER{" + userX.getName() + "] CREATED" + "\n=================\n");
				}
				
				User userY = new User();
				userY.setLogin("testUserY" + i);
				userY.setName("testUserY" + i);
				userY.setPassword("qwe123" + i);
				userY.setRole(2);
				userY.setRank(1);
				userY.setSpeciality(spec[1]);
				userY.setCreatingDate(Calendar.getInstance());
				userY.setEmail("testUserY" + (i + 1) + "@gmail.com");
				userService.register(userY);
				if (DEBUG) {
				System.out.println("\n=================\n" + "## : USER{" + userY.getName() + "] CREATED" + "\n=================\n");
				}
					
				GM.wannaplay(userX);
				GM.wannaplay(userY);
			}
		}
if (DEBUG) {
		System.out.println("==================== # LET'S START OUR FIRST GAME # ====================");
		System.out.println("\n=================\n" + "## : Game CREATED" + "\n=================\n");
}
//        GM.wannaplay(user);
//        GM.wannaplay(user1);
	}

}
