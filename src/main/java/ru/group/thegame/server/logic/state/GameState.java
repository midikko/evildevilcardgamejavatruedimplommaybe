/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic.state;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import ru.group.thegame.server.logic.event.Event;

/**
 *
 * @author Admin
 */
public class GameState implements Serializable{

	private final BoardState boardState;

	private List<PlayerState> playersState;

	private int turn;

	private int currentUser;
	
	private List<Event> events;

	public GameState(BoardState boardState) {
		this.boardState = boardState;
		turn = 1;
		currentUser = ThreadLocalRandom.current().nextInt(0, 2);
	}

	public GameState(GameState gameState) {
		this.boardState = new BoardState(gameState.getBoardState());
		this.playersState = new ArrayList<>(gameState.getPlayersState());
		this.turn = gameState.getTurn();
		this.currentUser = gameState.getCurrentUser();
	}

	public void setPlayersState(List<PlayerState> playersState) {
		this.playersState = playersState;
	}

	public int getTurn() {
		return turn;
	}

	public void incrTurn() {
		turn++;
	}

	public int getCurrentUser() {
		return currentUser;
	}

	public void changeCurrentUser() {
		currentUser = currentUser == 0 ? 1 : 0;
	}

	public BoardState getBoardState() {
		return boardState;
	}

	public List<PlayerState> getPlayersState() {
		return playersState;
	}	

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}
}
