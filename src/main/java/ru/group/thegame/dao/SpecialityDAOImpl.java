/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.dao;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.hibernate.Hibernate;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import ru.group.thegame.domain.Speciality;

/**
 *
 * @author potapov
 */
public class SpecialityDAOImpl implements SpecialityDAO{
        @Resource(name = "sessionFactory")
    protected SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected Session getSession() {
        return sessionFactory.openSession();
    }

    @Override
    public Speciality create(Speciality entity) {
        Session session = getSession();
        Long id = (Long) session.save(entity);
        if (session != null) {
            session.flush();
            session.close();
        }
        return findByID(id);
    }

    @Override
    public List findAll() {
        Session session = getSession();
        session.beginTransaction();
        String query = "from Speciality where del=NULL and id not in(select old_act from Speciality where old_act=0)";
        List<Speciality> entitys = new ArrayList<Speciality>(session.createQuery(query).list());
        session.getTransaction().commit();
        if (session != null) {
            session.flush();
            session.close();
        }
        return entitys;
    }

    @Override
    @Transactional
    public Speciality save(Speciality entity) {
        
        Session session = getSession();
		//Update action Details
        //Transaction tx = session.beginTransaction();
        //не знаю что писать, посему жду пояснений
        Speciality a = new Speciality(); //это не решение, а заглушка
        session.update(entity);
        //tx.commit();
        if (session != null) {
            session.flush();
            session.close();
        }
        return a;
    }

	@Override
	public void delete(Speciality entity) {
		Session session = getSession();
        //delete customer Details
		//Transaction tx = session.beginTransaction();
		session.delete(entity);
		//tx.commit();
		if (session != null) {
			session.flush();
			session.close();
		}
	}

	@Override
	public Speciality findByID(long id) {


        Session session = getSession();
        Speciality entity = (Speciality) session.load(Speciality.class, id);
        try {
            Hibernate.initialize(entity);
        } catch (ObjectNotFoundException e) {
            System.out.println("Speciality object with id " + id + " not found");
            entity = null;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return entity;
	}

	@Override
	public boolean isExist(Speciality entity) {

		Session session = getSession();
		Speciality entity1 = findByID(entity.getId());
		if (entity1 == null) {
			return false;
		};
		return true;
	}
}
