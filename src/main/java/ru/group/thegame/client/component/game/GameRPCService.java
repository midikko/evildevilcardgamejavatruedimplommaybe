/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.client.component.game;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import java.util.List;
import ru.group.thegame.shared.Card;
import ru.group.thegame.shared.User;
import ru.group.thegame.shared.Game;

/**
 *
 * @author MasterKrab
 */
@RemoteServiceRelativePath("springGwtServices/GameRPCService")
public interface GameRPCService extends RemoteService{
	
	public List<User> getOnlineUsers();
	
	public void startGame();
	
	public void leaveQueue();
	
	public List<Card> getCommonCards();
	
	public Long amIPlaying();
	
	public Game getGameByUID(long uid);
	
	public void surrender(long uid);

	public User getCurrentUser();

	public void putCard(Card selected, int j);
	
	public void skipTurn();
	
}
