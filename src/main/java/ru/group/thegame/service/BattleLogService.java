/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import java.util.List;
import ru.group.thegame.domain.BattleLog;

/**
 *
 * @author potapov
 */
public interface BattleLogService {
    
    public void register(BattleLog entity);

    public List<BattleLog> getAll();

    public BattleLog getById(long id);

    public void delete(BattleLog entity);

    public void save(BattleLog entity);
}
