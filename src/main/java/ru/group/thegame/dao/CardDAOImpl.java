/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.dao;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.hibernate.Hibernate;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import ru.group.thegame.domain.Card;
import ru.group.thegame.domain.Speciality;

/**
 *
 * @author potapov
 */
public class CardDAOImpl implements CardDAO {

	@Resource(name = "sessionFactory")
	protected SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	protected Session getSession() {
		return sessionFactory.openSession();
	}

	@Override
	public void create(Card entity) {
		Session session = getSession();
		session.save(entity);
		if (session != null) {
			session.flush();
			session.close();
		}
	}

	@Override
	public List findAll() {
		Session session = getSession();
		session.beginTransaction();
		String query = "from Card where del=NULL and id not in(select old_act from Card where old_act=0)"; //todo Вить проверь действительно ли такой запрос должен быть
		List<Card> entitys = new ArrayList<Card>(session.createQuery(query).list());
		session.getTransaction().commit();
		if (session != null) {
			session.flush();
			session.close();
		}
		return entitys;
	}

	@Override
	@Transactional
	public Card save(Card entity) {
		
		Session session = getSession();
		//Update action Details
		//Transaction tx = session.beginTransaction();
		//не знаю что писать, посему жду пояснений
		Card a = new Card(); //это не решение, а заглушка
		session.update(entity);
		//tx.commit();
		if (session != null) {
			session.flush();
			session.close();
		}
		return a;
	}

	@Override
	public void delete(Card entity) {
		Session session = getSession();
		//delete customer Details
		//Transaction tx = session.beginTransaction();
		session.delete(entity);
		//tx.commit();
		if (session != null) {
			session.flush();
			session.close();
		}
	}

	@Override
	public Card findByID(long id) {

		Session session = getSession();
		Card entity = (Card) session.load(Card.class, id);
		try {
			Hibernate.initialize(entity);
		} catch (ObjectNotFoundException e) {
			System.out.println("Card object with id " + id + " not found");
			entity = null;
		} finally {
			if (session != null) {
				session.flush();
				session.close();
			}
		}
		return entity;
	}

	@Override
	public boolean isExist(Card entity) {

		Session session = getSession();
		Card entity1 = findByID(entity.getId());
		if (entity1 == null) {
			return false;
		};
		return true;
	}

	@Override
	public List findCommon() {
		List<Card> entitys = new ArrayList<Card>();
		Session session = getSession();
		session.beginTransaction();
		Speciality speciality = (Speciality) session.load(Speciality.class, 1l);
		Query query = session.createQuery("from Card where speciality = :speciality ");
		query.setParameter("speciality", speciality);
		entitys.addAll(query.list());

		speciality = (Speciality) session.load(Speciality.class, 2l);
		query = session.createQuery("from Card where speciality = :speciality ");
		query.setParameter("speciality", speciality);
		entitys.addAll(query.list());

		speciality = (Speciality) session.load(Speciality.class, 3l);
		query = session.createQuery("from Card where speciality = :speciality ");
		query.setParameter("speciality", speciality);
		entitys.addAll(query.list());

		speciality = (Speciality) session.load(Speciality.class, 4l);
		query = session.createQuery("from Card where speciality = :speciality ");
		query.setParameter("speciality", speciality);
		entitys.addAll(query.list());

		session.getTransaction().commit();
		if (session != null) {
			session.flush();
			session.close();
		}
		return entitys;
	}

	@Override
	public List findBySpeciality(Speciality speciality) {
		Session session = getSession();
		session.beginTransaction();
		Query query = session.createQuery("from Card where speciality = :speciality ");
		query.setParameter("speciality", speciality);
		List<Card> entitys = new ArrayList<>(query.list());
		session.getTransaction().commit();
		if (session != null) {
			session.flush();
			session.close();
		}
		return entitys;
	}
}
