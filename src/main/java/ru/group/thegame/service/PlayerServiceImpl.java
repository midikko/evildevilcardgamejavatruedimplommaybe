/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import ru.group.thegame.shared.Card;
import ru.group.thegame.shared.Player;

public class PlayerServiceImpl implements PlayerService {

	@Autowired
	UserService userService;

	@Autowired
	CardService cardService;

	@Override
	public Player fromLogicToShared(ru.group.thegame.server.logic.Player p) {
		Player player = new Player();
		List<Card> list = new ArrayList<>();
		for (ru.group.thegame.server.logic.Card card : p.getPlayerState().getCards()) {
			list.add(cardService.fromLogicToShared(card));
		}
		player.setCards(list);
		player.setHealth(p.getPlayerState().getHealth());
		player.setPowers(p.getPlayerState().getPowers());
		player.setUser(userService.init(p.getUser()));
		return player;
	}

}
