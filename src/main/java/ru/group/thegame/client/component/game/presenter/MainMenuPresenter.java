/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.component.game.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import ru.group.thegame.client.Presenter;
import ru.group.thegame.client.component.game.GameRPCService;
import ru.group.thegame.client.component.game.GameRPCServiceAsync;
import ru.group.thegame.client.event.OpenComponentEvent;
import ru.group.thegame.shared.User;

/**
 *
 * @author MasterKrab
 */
public class MainMenuPresenter implements Presenter {

	public interface Display {

		public void setData(List<User> list);

		public TextBox getSearch();

		public Button getStartGame();

		public Button getRefresh();

		public HasClickHandlers getClear();

		public Button getCancel();

		public Widget asWidget();

		public void setUserName(String userName);

		public void setLvl(String lvl);

		public void setWins(String wins);

		public void setLoses(String loses);

		public void setAvatar(String url);
	}

	private final Display display;

	private final HandlerManager eventBus;

	private final GameRPCServiceAsync rpcService;

	Timer t;

	User currentUser;

	List<User> onlineUsers;

	public MainMenuPresenter(Display display, HandlerManager eventBus) {
		this.display = display;
		this.eventBus = eventBus;
		this.rpcService = GWT.create(GameRPCService.class);
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(display.asWidget());
		fetchOnlineUsers();
		bind();
	}

	private void fetchOnlineUsers() {
		rpcService.getOnlineUsers(new AsyncCallback<List<User>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Fail by getting users from queue");
			}

			@Override
			public void onSuccess(List<User> result) {
				onlineUsers = result;
				display.setData(result);
			}
		});
		rpcService.getCurrentUser(new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {
				throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
			}

			@Override
			public void onSuccess(User result) {
				currentUser = result;
				display.setAvatar(currentUser.getAvatarUrl());
				display.setUserName(currentUser.getName());
				display.setLvl(currentUser.getRank() + "");
				display.setWins("1");
				display.setLoses("3");
			}
		});
	}

	private void bind() {
		display.getClear().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				display.getSearch().setText("");
			}
		});
		display.getSearch().addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				final String token = display.getSearch().getText();
				List<User> list = new ArrayList<>();
				for (User u : onlineUsers) {
					if (u.getLogin().contains(token)) {
						list.add(u);
					}
				}
				display.setData(list);

			}
		});

		display.getStartGame().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				rpcService.startGame(new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Fails start game");
					}

					@Override
					public void onSuccess(Void result) {
						Window.alert("Your Game will start soon");
						display.getStartGame().setEnabled(false);
						display.getCancel().setEnabled(true);
						checkGame();
						fetchOnlineUsers();
					}
				});
			}
		});

		display.getCancel().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				rpcService.leaveQueue(new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
					}

					@Override
					public void onSuccess(Void result) {
						display.getCancel().setEnabled(false);
						display.getStartGame().setEnabled(true);
						t.cancel();
						fetchOnlineUsers();
					}
				});
			}
		});

		display.getRefresh().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				rpcService.amIPlaying(new AsyncCallback<Long>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert(this.getClass().toString() + "\n\ncheckGame()=>amIPlaying fails");
					}

					@Override
					public void onSuccess(Long result) {
						if (result == -1) {
							fetchOnlineUsers();
						} else {
							eventBus.fireEvent(new OpenComponentEvent("game-game-" + result));
						}
					}
				});
			}
		});
	}

	private void checkGame() {
		t = new Timer() {

			@Override
			public void run() {
				rpcService.amIPlaying(new AsyncCallback<Long>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert(this.getClass().toString() + "\n\ncheckGame()=>amIPlaying fails");
					}

					@Override
					public void onSuccess(Long result) {
						if (result == -1) {
							fetchOnlineUsers();
							checkGame();
						} else {
							eventBus.fireEvent(new OpenComponentEvent("game-game-" + result));
						}
					}
				});
			}
		};
		t.schedule(10000);
	}
}
