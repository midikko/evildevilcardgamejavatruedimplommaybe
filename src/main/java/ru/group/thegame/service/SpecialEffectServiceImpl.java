/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import ru.group.thegame.dao.SpecialEffectDAO;
import ru.group.thegame.domain.SpecialEffect;

/**
 *
 * @author potapov
 */
public class SpecialEffectServiceImpl implements SpecialEffectService{

    @Autowired
    private SpecialEffectDAO specialEffectDao;
    
    @Override
    public void register(SpecialEffect entity) {
        specialEffectDao.create(entity);
    }

    @Override
    public List<SpecialEffect> getAll() {
        return specialEffectDao.findAll();
    }

    @Override
    public SpecialEffect getById(long id) {
        return specialEffectDao.findByID(id);
    }

    @Override
    public void delete(SpecialEffect entity) {
        specialEffectDao.delete(entity);
    }

    @Override
    public void save(SpecialEffect entity) {
        specialEffectDao.save(entity);
    }

    @Override
    public ru.group.thegame.shared.SpecialEffect fromDomainToShared(SpecialEffect entity) {
        ru.group.thegame.shared.SpecialEffect shar = new ru.group.thegame.shared.SpecialEffect();
        shar.setDescr(entity.getDescr());
        shar.setMethod(entity.getMethod());
        shar.setName(entity.getName());
        shar.setPower(entity.getPower());
        shar.setType(entity.getType());
        shar.setId(entity.getId());
        return shar;
    }

    @Override
    public SpecialEffect fromSharedToDomain(ru.group.thegame.shared.SpecialEffect entity) {
        SpecialEffect domain = new SpecialEffect();
        domain.setDescr(entity.getDescr());
        domain.setMethod(entity.getMethod());
        domain.setName(entity.getName());
        domain.setPower(entity.getPower());
        domain.setType(entity.getType());
        domain.setId(entity.getId());
        return domain;
    }

	@Override
	public ru.group.thegame.server.logic.SpecialEffect fromDomainToLogic(SpecialEffect entity) {
		ru.group.thegame.server.logic.SpecialEffect logic = new ru.group.thegame.server.logic.SpecialEffect();
        logic.setDescr(entity.getDescr());
        logic.setMethod(entity.getMethod());
        logic.setName(entity.getName());
        logic.setPower(entity.getPower());
        logic.setType(entity.getType());
        return logic;
	}

	@Override
	public ru.group.thegame.server.logic.SpecialEffect fromSharedToLogic(ru.group.thegame.shared.SpecialEffect entity) {
		ru.group.thegame.server.logic.SpecialEffect logic = new ru.group.thegame.server.logic.SpecialEffect();
        logic.setDescr(entity.getDescr());
        logic.setMethod(entity.getMethod());
        logic.setName(entity.getName());
        logic.setPower(entity.getPower());
        logic.setType(entity.getType());
        return logic;
	}

	@Override
	public ru.group.thegame.shared.SpecialEffect fromLogicToShared(ru.group.thegame.server.logic.SpecialEffect entity) {
		ru.group.thegame.shared.SpecialEffect shar = new ru.group.thegame.shared.SpecialEffect();
        shar.setDescr(entity.getDescr());
        shar.setMethod(entity.getMethod());
        shar.setName(entity.getName());
        shar.setPower(entity.getPower());
        shar.setType(entity.getType());
        return shar;
	}
}
