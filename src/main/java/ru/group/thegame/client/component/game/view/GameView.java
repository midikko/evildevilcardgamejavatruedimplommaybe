/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.component.game.view;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import java.util.ArrayList;
import java.util.List;
import ru.group.thegame.client.component.game.presenter.GamePresenter;
import ru.group.thegame.shared.Card;
import ru.group.thegame.shared.Game;
import ru.group.thegame.shared.Player;

/**
 *
 * @author MasterKrab
 */
public class GameView extends Composite implements GamePresenter.Display {

	private FlowPanel gameZone = new FlowPanel();
	private FlowPanel controlPanel = new FlowPanel();
	private FlowPanel avalibleCards = new FlowPanel();
	private FlowPanel users = new FlowPanel();
	private FlowPanel player1 = new FlowPanel();
	private FlowPanel player2 = new FlowPanel();
	private FlowPanel timersPanel = new FlowPanel();
	private FlowPanel buttonsPanel = new FlowPanel();
	private List<FlowPanel> playersList = new ArrayList<>();
	private List<FlowPanel> cardsPanelList = new ArrayList<>();

	private FlowPanel board = new FlowPanel();
	private FlowPanel cards1Panel = new FlowPanel();
	private FlowPanel cards2Panel = new FlowPanel();

	private List<FocusPanel> cards1 = new ArrayList<>();
	private List<FocusPanel> cards2 = new ArrayList<>();
	private List<FocusPanel> cards = new ArrayList<>();

	private Button surrender = new Button("Surrender");
	private Button skip = new Button("Skip");
	private Button refresh = new Button("Refresh");

	private Label turn = new Label();
	private Label currentPlayer = new Label();
	private Label timer = new Label();
	private Label timerOver = new Label();

	private final long id;
	private long currentUser = -1;// id в игре (0 или 1) пользователя в игре
	private Player player;

	public GameView(long id) {
		this.id = id;

		initWidget(gameZone);

		gameZone.addStyleName("game-zone");
		controlPanel.addStyleName("control-panel");
		timer.addStyleName("timer");
		currentPlayer.addStyleName("current-player");
		turn.addStyleName("turn");
		skip.addStyleName("button skip");
		surrender.addStyleName("button surrender");
		refresh.addStyleName("button refresh");
		avalibleCards.addStyleName("avalible-cards");
		player1.addStyleName("player");
		player1.getElement().setId("player-1");
		player2.addStyleName("player");
		player1.getElement().setId("player-2");
		board.addStyleName("board");
		cards1Panel.addStyleName("cards-panel panel1");
		cards2Panel.addStyleName("cards-panel panel2");
		users.addStyleName("user");
		timersPanel.addStyleName("timers-panel");
		timerOver.addStyleName("timer-over");
		buttonsPanel.addStyleName("buttons-row");

		users.add(player1);
		users.add(player2);

		gameZone.add(controlPanel);
		gameZone.add(users);
		gameZone.add(board);
		gameZone.add(avalibleCards);

		timersPanel.add(timer);
		timersPanel.add(timerOver);

		buttonsPanel.add(surrender);
		buttonsPanel.add(skip);
		buttonsPanel.add(refresh);

		controlPanel.add(currentPlayer);
		controlPanel.add(timersPanel);
		controlPanel.add(turn);

		controlPanel.add(buttonsPanel);

		playersList.add(player1);
		playersList.add(player2);

		cardsPanelList.add(cards1Panel);
		cardsPanelList.add(cards2Panel);

		board.add(cards1Panel);
		board.add(cards2Panel);
	}

	@Override
	public void setData(Game game) {
		avalibleCards.clear();
		player1.clear();
		player2.clear();

		cards1Panel.clear();
		cards2Panel.clear();

		cards1.clear();
		cards2.clear();
		cards.clear();

		player = game.getPlayers().get((int) currentUser);

		createPlayers(game.getPlayers());
		if (currentUser == 0) {
			createBoard(game.getBoard().getUser2Cards(), game.getBoard().getUser1Cards());
		} else {
			createBoard(game.getBoard().getUser1Cards(), game.getBoard().getUser2Cards());
		}
		createAvalibleCards(game.getPlayers().get((int) currentUser).getCards());
		currentPlayer.setText(game.getPlayers().get(game.getCurrentUser()).getUser().getLogin() + "'s turn");
		turn.setText("Turn : " + game.getTurn());
	}

	private void createPlayers(List<Player> players) {
		for (int j = 0; j < 2; j++) {
			int i;
			if (j == 0) {
				i = currentUser == 0 ? 1 : 0;
			} else {
				i = (int) currentUser;
			}
			Player p = players.get(i);
			Image avatar = new Image(p.getUser().getAvatarUrl());
			avatar.addStyleName("ava");

			FlowPanel userInfo = new FlowPanel();
			userInfo.addStyleName("user-info");

			Label login = new Label(p.getUser().getLogin());
			login.addStyleName("username-battle");
			userInfo.add(login);

			Label rank = new Label(p.getUser().getRank() + "");
			rank.addStyleName("rank");
			userInfo.add(rank);

			FlowPanel healthInfo = new FlowPanel();
			healthInfo.addStyleName("heath-info");

			Label healthLabel = new Label("Heath");
			healthLabel.addStyleName("health-label");
			healthInfo.add(healthLabel);

			Label health = new Label(p.getHealth() + "");
			health.addStyleName("health");
			healthInfo.add(health);

			FlowPanel power1Div = new FlowPanel();
			power1Div.addStyleName("row-power row-fire");

			List<Integer> powers = p.getPowers();

			Label power1 = new Label(powers.get(0) + "");
			power1.addStyleName("power fire");
			Label power1Name = new Label("Fire: ");
			power1.addStyleName("power-name");
			power1Div.add(power1Name);
			power1Div.add(power1);

			FlowPanel power2Div = new FlowPanel();
			power2Div.addStyleName("row-power row-water");

			Label power2 = new Label(powers.get(1) + "");
			power2.addStyleName("power water");
			Label power2Name = new Label("Water: ");
			power2.addStyleName("power-name");
			power2Div.add(power2Name);
			power2Div.add(power2);

			FlowPanel power3Div = new FlowPanel();
			power3Div.addStyleName("row-power row-air");

			Label power3 = new Label(powers.get(2) + "");
			power3.addStyleName("power air");
			Label power3Name = new Label("Air: ");
			power3.addStyleName("power-name");
			power3Div.add(power3Name);
			power3Div.add(power3);

			FlowPanel power4Div = new FlowPanel();
			power4Div.addStyleName("row-power row-earth");

			Label power4 = new Label(powers.get(3) + "");
			power4.addStyleName("power earth");
			Label power4Name = new Label("Earth: ");
			power4.addStyleName("power-name");
			power4Div.add(power4Name);
			power4Div.add(power4);

			FlowPanel power5Div = new FlowPanel();
			power5Div.addStyleName("row-power row-" + p.getUser().getSpeciality().getName());

			Label power5 = new Label(powers.get(4) + "");
			power5.addStyleName("power");
			Label power5Name = new Label(p.getUser().getSpeciality().getName() + ": ");
			power5.addStyleName("power-name");
			power5Div.add(power5Name);
			power5Div.add(power5);

			playersList.get(j).add(avatar);
			playersList.get(j).add(userInfo);
			playersList.get(j).add(healthInfo);
			playersList.get(j).add(power1Div);
			playersList.get(j).add(power2Div);
			playersList.get(j).add(power3Div);
			playersList.get(j).add(power4Div);
			playersList.get(j).add(power5Div);
		}
	}

	private void createBoard(List<Card>... list) {
		for (int k = 0; k < 2; k++) {
			int j;
			List<FocusPanel> cards;
			if (k == 0) {
				j = currentUser == 0 ? 1 : 0;
				cards = cards2;
			} else {
				j = (int) currentUser;
				cards = cards1;
			}
			for (int i = 0; i < 6; i++) {
				FocusPanel panel = new FocusPanel();
				panel.addStyleName("card");
				panel.getElement().setId("card-" + i);
				FlowPanel cardPanel = new FlowPanel();
				if (!list[j].isEmpty() && list[j].get(i) != null) {
					cardPanel.add(new Image(list[j].get(i).getImg()));

					Label health = new Label(list[j].get(i).getCurrentHealth() + "");
					health.addStyleName("icon icon-health");

					Label damage = new Label(list[j].get(i).getDamage() + "");
					damage.addStyleName("icon icon-damage");

					cardPanel.add(health);
					cardPanel.add(damage);

					panel.add(cardPanel);
				} else {
					panel.addStyleName("empty");
				}
				cards.add(panel);
				cardsPanelList.get(j).add(cards.get(cards.size() - 1));
			}
		}
	}

	private void createAvalibleCards(List<Card> cards) {
		for (int i = 0; i < cards.size() / 4; i++) {
			FlowPanel specialityPanel = new FlowPanel();
			specialityPanel.addStyleName("speciality-panel " + cards.get(i * 4).getSpeciality().getName());
			specialityPanel.getElement().setId("speciality-panel-" + i);
			for (int j = i * 4; j < i * 4 + 4; j++) {
				FocusPanel card = new FocusPanel();
				card.addStyleName("avalible-card " + cards.get(j).getSpeciality().getName());
				List<Integer> powers = player.getPowers();
				int power;
				if (cards.get(j).getSpeciality().getId() < 4) {
					power = (int) cards.get(j).getSpeciality().getId();
				} else {
					power = 4;
				}
				if (cards.get(j).getCost() > powers.get(power)) {
					card.addStyleName("non-avalible");
				} else {
					card.addStyleName("avalible");
				}
				FlowPanel cardPanel = new FlowPanel();

				card.addStyleName("avalible-card " + cards.get(j).getSpeciality().getName());

				cardPanel.add(new Image(cards.get(j).getPrev()));
				Label cost = new Label(cards.get(j).getCost() + "");
				cost.addStyleName("icon icon-cost");

				Label health = new Label(cards.get(j).getHealth() + "");
				health.addStyleName("icon icon-health");

				Label damage = new Label(cards.get(j).getDamage() + "");
				damage.addStyleName("icon icon-damage");

				cardPanel.add(cost);
				cardPanel.add(damage);
				cardPanel.add(health);

				card.add(cardPanel);
				this.cards.add(card);
				specialityPanel.add(this.cards.get(this.cards.size() - 1));
			}
			avalibleCards.add(specialityPanel);
		}
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public Button getSurrender() {
		return surrender;
	}

	@Override
	public Button getSkip() {
		return skip;
	}

	@Override
	public Button getRefresh() {
		return refresh;
	}

	@Override
	public List<FocusPanel> getCards() {
		return cards;
	}

	@Override
	public List<FocusPanel> getMyCards() {
		return currentUser == 0 ? cards2 : cards1;
	}

	@Override
	public void setTimer(String time) {
		timer.setText(time);
		if (time.equals("0")) {
			timerOver.setText("Time is over");
		} else {
			timerOver.setText("");
		}
	}

	@Override
	public void setCurrentUser(long id) {
		currentUser = id;
	}
}
