/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic.state;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import ru.group.thegame.server.logic.Card;

/**
 *
 * @author Admin
 */
public class BoardState implements Serializable{
	
	private final List<List<Card>> usersCards;

	public BoardState(int size) {
		usersCards = new ArrayList<>();
		usersCards.add(new ArrayList<Card>(size));
		usersCards.add(new ArrayList<Card>(size));
	}	

	public BoardState(BoardState boardState) {
		usersCards = new ArrayList<>(boardState.getUsersCards());
	}
	

	public List<List<Card>> getUsersCards() {
		return usersCards;
	}
}
