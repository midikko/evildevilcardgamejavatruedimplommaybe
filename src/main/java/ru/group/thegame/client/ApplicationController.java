/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Image;
import ru.group.thegame.client.event.OpenComponentEvent;
import ru.group.thegame.client.event.OpenComponentEventHandler;

/**
 *
 * @author Nevezhin_Pavel
 */
public class ApplicationController implements ValueChangeHandler<String> {

	final private HasWidgets container;
	private final HandlerManager eventBus;

	FlowPanel header = new FlowPanel();
	FlowPanel content = new FlowPanel();
	FlowPanel footer = new FlowPanel();

	FlowPanel menu = new FlowPanel();

	ru.group.thegame.client.component.menu.ComponentController menuController;
	ru.group.thegame.client.component.game.ComponentController gameController;
	ru.group.thegame.client.component.site.ComponentController siteController;

	Image logo = new Image("img/logo.png");

	public ApplicationController(HasWidgets container, HandlerManager eventBus) {
		this.container = container;
		this.eventBus = eventBus;
	}

	public void go() {

		header.addStyleName("header");
		header.add(logo);
		header.add(menu);

		//add style
		logo.addStyleName("logo");
		menu.addStyleName("menu");
		content.addStyleName("content");
		footer.addStyleName("footer");

		container.add(header);
		container.add(content);
		container.add(footer);

		init();
		bind();

		menuController.go(menu);
		
		String[] tokenParts;
		if (History.getToken().equals("")) {
			openComponent("site");
		} else if (History.getToken().contains("-")) {
			tokenParts = History.getToken().split("-");
			History.newItem(tokenParts[0]);
			openPage(tokenParts[0]);
		} else {
			openPage(History.getToken());
		}
	}

	private void openComponent(String token) {
		History.newItem(token);
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();
		openPage(token);

	}

	private void bind() {
		History.addValueChangeHandler(this);
		eventBus.addHandler(OpenComponentEvent.TYPE, new OpenComponentEventHandler() {
			@Override
			public void onOpenComponent(OpenComponentEvent event) {
				openComponent(event.target);
			}
		});
	}

	private void init() {
		menuController = new ru.group.thegame.client.component.menu.ComponentController(eventBus);

		siteController = new ru.group.thegame.client.component.site.ComponentController(eventBus);
		History.addValueChangeHandler((ValueChangeHandler<String>) siteController);

		gameController = new ru.group.thegame.client.component.game.ComponentController(eventBus);
		History.addValueChangeHandler((ValueChangeHandler<String>) gameController);
	}

	private void openPage(String token) {
		if (token != null) {
			switch (token) {
				case "site":
					siteController.go(content);
					break;
				case "game":
					gameController.go(content);
					break;
			}
		}
	}

}
