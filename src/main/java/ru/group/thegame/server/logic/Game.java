/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import ru.group.thegame.Initiator;

import ru.group.thegame.domain.BattleLog;
import ru.group.thegame.server.logic.event.CardAdditionEvent;
import ru.group.thegame.server.logic.event.CardDeathEvent;
import ru.group.thegame.server.logic.event.CardHealthChangeEvent;
import ru.group.thegame.server.logic.event.PlayerHealthChangeEvent;
import ru.group.thegame.server.logic.event.PlayerPowerChangeEvent;
import ru.group.thegame.server.logic.state.GameState;
import ru.group.thegame.service.CardService;

/**
 *
 * @author Nevezhin_Pavel
 */
public class Game implements Comparable<Game> {

	private final GameManager GM;

	private final List<GameState> gameStatesLog;

	private final CardService cardService;

	private final long UID;

	private final Board board;

	private final List<Player> players = new ArrayList<>(2);

	private final Calendar startTime;

	private Calendar endTime;

	private Thread t;

	private int time;

	private boolean flag = true;

	private final BattleLog log = new BattleLog();

	private final GameState gameState;

	public Game(Player player1, Player player2, GameManager GM, CardService cardService) {
		startTime = Calendar.getInstance();
		board = new Board();

		if (Initiator.DEBUG) {
			System.out.println("\n\nDEBUG GM  IN GAME.java :: " + GM + "\n");
			System.out.println("\n\nDEBUG Board  IN GAME.java :: " + board + "\n");
			System.out.println("DEBUG cardService  IN GAME.java :: " + cardService + "\n");
		}

		UID = GM.getUID();

		if (Initiator.DEBUG) {
			System.out.println("DEBUG game  IN GAME.java :: " + this + "  ::  " + UID + "\n");
		}

		players.add(player1);
		players.add(player2);
		log.setLog("");
		this.cardService = cardService;
		this.GM = GM;
		gameState = new GameState(board.getBoardState());

		gameStatesLog = new ArrayList<>();
	}

	public GameState getGameState() {
		return gameState;
	}

	public Board getBoard() {
		return board;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public Calendar getStartTime() {
		return startTime;
	}

	public Calendar getEndTime() {
		return endTime;
	}

	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}

	public long getUID() {
		return UID;
	}

	public int getTime() {
		return time;
	}

	@Override
	public int compareTo(Game o) {
		return this.getStartTime().compareTo(o.getStartTime());
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + Objects.hashCode(this.startTime);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Game other = (Game) obj;
		if (this.UID != other.UID) {
			return false;
		}
		if (!Objects.equals(this.startTime, other.startTime)) {
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * Запуск хода</p>
	 * <p>
	 * Определяется текущий пользователь, из доски извлекаются активные
	 * карты(все), пробегаемся по всем картам текущего пользователя, запускаем
	 * их спецэффект onTurn и обычную атаку, по завершению меняем текущего
	 * пользователя, увеличиваем счетчик хода</p>
	 */
	private GameState nextTurn() {

		int currentUser = gameState.getCurrentUser();
		int anotherUser = currentUser == 0 ? 1 : 0;
		Player enemy = players.get(anotherUser);
		List<Card> userCards = board.getUserCards(currentUser);
		List<Card> userCards1 = board.getUserCards(anotherUser);
		for (int i = 0; i < 6; i++) {
			Card currentCard = userCards.get(i);
			if (currentCard != null) {
				currentCard.onTurn();

				Card enemyCard = userCards1.get(i);
				int damage = currentCard.getDamage();
				if (damage != 0) {
					if ((enemyCard == null)) {
						int enemyHealth = enemy.getPlayerState().getHealth() - damage;
						enemy.getPlayerState().setHealth(enemyHealth);
						
						gameState.getEvents().add(new PlayerHealthChangeEvent(anotherUser, damage));
						
						log.setLog(log.getLog() + "\nUser " + enemy.getUser().getLogin() + " was hitted and get " + damage + " damage by " + currentCard.getName() + ".  his current health equals to '" + enemy.getPlayerState().getHealth() + "'.");
					} else {
						int enemyCardHealth = enemyCard.getCardState().getCurrentHealth() - damage;
						
						gameState.getEvents().add(new CardHealthChangeEvent(anotherUser, enemyCard.getCardState().getPosition(), damage));
						
						log.setLog(log.getLog() + "\n" + players.get(currentUser).getUser().getLogin() + "'s card named" + currentCard.getName() + " deals " + damage + "damage to " + enemyCard.getName());
						if (enemyCardHealth < 1) {
							
							gameState.getEvents().add(new CardDeathEvent(anotherUser, enemyCard.getCardState().getPosition()));
							
							log.setLog(log.getLog() + "And " + enemyCard.getName() + " died.");
							enemyCard.removeCard();
						} else {
							log.setLog(log.getLog() + "And it's current healt equals " + enemyCard.getCardState().getCurrentHealth());
							enemyCard.getCardState().setCurrentHealth(enemyCardHealth);
						}
					}
				}
			}
		}

		for (int i = 0; i < 5; i++) {
			
			gameState.getEvents().add(new PlayerPowerChangeEvent(anotherUser, i, 1));
			
			players.get(anotherUser).getPlayerState().getPowers().set(i, players.get(anotherUser).getPlayerState().getPowers().get(i) + 1);
		}

		if (enemy.getPlayerState().getHealth() < 1) {
			t.interrupt();
			flag = false;
			endTime = Calendar.getInstance();
			log.setCreatingDate(startTime);
			log.setDeck1(players.get(0).getUser().getSpeciality().getId());
			log.setDeck2(players.get(1).getUser().getSpeciality().getId());
			log.setEndDate(endTime);
			log.setLog(log.getLog() + "\nUser " + players.get(currentUser).getUser().getLogin() + " from card damage.");
			log.setUser1(players.get(0).getUser());
			log.setUser2(players.get(1).getUser());
			log.setUserWin(players.get(currentUser).getUser());
			GM.endGame(this, log);
			return gameState;
		}

		gameState.changeCurrentUser();
		gameState.incrTurn();
		return gameState;
	}

	/**
	 * <p>
	 * Кладет карту на стол</p>
	 * <p>
	 * Добавление карты в список активных карт игрока, запуск onActivation
	 * спецэффекта карты и запуск следующего хода</p>
	 *
	 * @param position Куда кладется карта
	 * @param card объект карты, которая кладется
	 */
	public void putCard(int position, Card card) {
		List<Integer> powers = players.get(gameState.getCurrentUser()).getPlayerState().getPowers();
		int power;
		if (card.getSpeciality().getId() < 4) {
			power = (int) card.getSpeciality().getId();
		} else {
			power = 4;
		}
		
		gameState.getEvents().add(new PlayerPowerChangeEvent(card.getOwner(), power, card.getCost()));		
		
		powers.set(power, powers.get(power) - card.getCost());
		
		gameState.getEvents().add(new CardAdditionEvent(card.getOwner(), card.getCardState()));
		
		card.putCard(position);
		log.setLog(log.getLog() + "\nCard " + card.getName() + " was placed by " + players.get(card.getOwner()).getUser().getLogin() + "");
		log.setLog(log.getLog() + "\nCard used it's 'on activate' skill and " + (card.getOnActiv().getType().equals("damage") ? "deals " : "heals ") + card.getOnActiv().getPower() + (card.getOnActiv().getType().equals("damage") ? " damage." : " hp to " + card.getOnActiv().getMethod()));
		flag = false;
	}

	/**
	 * <p>
	 * Пропуск хода.</p>
	 * <p>
	 * Просто запускает следующий ход</p>
	 */
	public void skip() {
		flag = false;
	}

	/**
	 * <p>
	 * Добровольное поражение.</p>
	 * <p>
	 * Формирует лог и заканчивает игру. Текущий пользователь объявляется
	 * проигравшим.</p>
	 */
	public void surrender() {
		t.interrupt();
		flag = false;
		endTime = Calendar.getInstance();
		log.setCreatingDate(startTime);
		log.setDeck1(players.get(0).getUser().getSpeciality().getId());
		log.setDeck2(players.get(1).getUser().getSpeciality().getId());
		log.setLog(log.getLog() + "\nUser " + players.get(gameState.getCurrentUser()).getUser().getLogin() + " surrended. Game ended.");
		log.setEndDate(endTime);
		log.setUser1(players.get(0).getUser());
		log.setUser2(players.get(1).getUser());
		log.setUserWin(players.get(gameState.getCurrentUser() == 0 ? 1 : 0).getUser());
		GM.endGame(this, log);
	}

	public void start() {
		BoardManager bm = new BoardManager(board);
		List<Card> common = new ArrayList<>();

		for (ru.group.thegame.domain.Card c : cardService.getCommon()) {
			common.add(cardService.fromDomainToLogic(c));
		}
		if (Initiator.DEBUG) {
			System.out.println("\n\n" + common.size() + "\n\n");
		}
//при учете что есть 12 карт в каждой колоде
		for (int j = 0; j < 2; j++) {
			List<Card> cards = new ArrayList<>();
			for (int i = 0; i < 4; i++) {
				Card commonCard = common.get(i * 12 + ThreadLocalRandom.current().nextInt(0, 2));
				commonCard.setOwner(j);
				commonCard.setBoardManager(bm);
				cards.add(commonCard);

				commonCard = common.get(i * 12 + ThreadLocalRandom.current().nextInt(2, 6));
				commonCard.setOwner(j);
				commonCard.setBoardManager(bm);
				cards.add(commonCard);

				commonCard = common.get(i * 12 + ThreadLocalRandom.current().nextInt(6, 10));
				commonCard.setOwner(j);
				commonCard.setBoardManager(bm);
				cards.add(commonCard);

				commonCard = common.get(i * 12 + ThreadLocalRandom.current().nextInt(10, 12));
				commonCard.setOwner(j);
				commonCard.setBoardManager(bm);
				cards.add(commonCard);
			}

			List<Card> specialCards = new ArrayList<>();
			for (ru.group.thegame.domain.Card c : cardService.getBySpeciality(players.get(j).getUser().getSpeciality())) {
				specialCards.add(cardService.fromDomainToLogic(c));
			}

			Card get = specialCards.get(ThreadLocalRandom.current().nextInt(0, 2));
			get.setOwner(j);
			get.setBoardManager(bm);
			cards.add(get);
			get = specialCards.get(ThreadLocalRandom.current().nextInt(2, 6));
			get.setOwner(j);
			get.setBoardManager(bm);
			cards.add(get);
			get = specialCards.get(ThreadLocalRandom.current().nextInt(6, 10));
			get.setOwner(j);
			get.setBoardManager(bm);
			cards.add(get);
			get = specialCards.get(ThreadLocalRandom.current().nextInt(10, 12));
			get.setOwner(j);
			get.setBoardManager(bm);
			cards.add(get);

			players.get(j).getPlayerState().setCards(cards);
		}
		t = new Thread() {

			@Override
			public void run() {
				outerloop:
				while (!isInterrupted()) {
					time = 80;
					while (flag && time > 0) {
						try {
							sleep(500);
							time--;
						} catch (InterruptedException ex) {
							flag = false;
							break outerloop;
						}
					}
					flag = true;
					gameStatesLog.add(nextTurn());
				}
			}
		};

		t.start();
	}
}
