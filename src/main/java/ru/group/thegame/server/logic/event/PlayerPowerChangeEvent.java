/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic.event;

import java.io.Serializable;

/**
 *
 * @author nevezhinpavel
 */
public class PlayerPowerChangeEvent implements Event, Serializable{
	
	private int player;
	
	private int power;
	
	private int powerChange;

	public PlayerPowerChangeEvent(int player, int power, int powerChange) {
		this.player = player;
		this.power = power;
		this.powerChange = powerChange;
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

	public int getPowerChange() {
		return powerChange;
	}

	public void setPowerChange(int powerChange) {
		this.powerChange = powerChange;
	}
	
}
