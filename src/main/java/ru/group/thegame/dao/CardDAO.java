/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.dao;

import java.util.List;
import ru.group.thegame.domain.Card;
import ru.group.thegame.domain.Speciality;

/**
 *
 * @author potapov
 */
public interface CardDAO {
    public void create(Card entity);

    public Card save(Card entity);

    public void delete(Card entity);

    public List findAll();
	
	public List findCommon();
	
	public List findBySpeciality(Speciality speciality);

    public Card findByID(long id);
    
    public boolean isExist(Card entity);
}
