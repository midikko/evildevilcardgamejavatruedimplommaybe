/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.server.logic;

import org.springframework.beans.factory.annotation.Autowired;
import ru.group.thegame.service.CardService;

/**
 *
 * @author potapov
 */
public class GameInitiator {
    @Autowired
    GameManager GM;
    @Autowired
    CardService cardService;
    
    public Game letusplay(Player player1, Player player2){
        Game game  = new Game(player1, player2, GM, cardService);
        return game;
    }
    
}
