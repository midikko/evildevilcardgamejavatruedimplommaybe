/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.component.game.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import ru.group.thegame.client.component.game.presenter.MainMenuPresenter;
import ru.group.thegame.shared.User;

/**
 *
 * @author MasterKrab
 */
public class MainMenuView extends Composite implements MainMenuPresenter.Display {

	FlowPanel container = new FlowPanel();

	FlowPanel playerQueue = new FlowPanel();
	
	FlowPanel userPanel = new FlowPanel();

	FlowPanel overQueue = new FlowPanel();
	
	FlowPanel userNameRow = new FlowPanel();
	
	FlowPanel userLvlRow = new FlowPanel();
	
	FlowPanel userWinsRow = new FlowPanel();
	
	FlowPanel userLosesRow = new FlowPanel();
	
	TextBox search = new TextBox();
	Button clear = new Button("Clear");
	Button startGame = new Button("Start");
	Button refresh = new Button("Refresh");
	Button cancel = new Button("Cancel");
	Label userNameValue = new Label("");
	Label lvlValue = new Label("");
	Label winsValue = new Label("");
	Label losesValue = new Label("");
	Image ava = new Image();
	
	

//	UserCell userCell = new UserCell();
//	CellList<User> userList = new CellList<>(userCell);
//	final ListDataProvider<User> dataProvider = new ListDataProvider<>();
	ListBox userList = new ListBox(true);

	public MainMenuView() {
		initWidget(container);

		container.add(userPanel);
		container.add(playerQueue);
		container.add(startGame);
		container.add(refresh);
		container.add(cancel);
		cancel.setEnabled(false);
		
		Label userName = new Label("UserName");
		Label lvl = new Label("Lvl");
		Label wins = new Label("Wins");
		Label loses = new Label("Loses");
		
		
		userNameRow.add(userName);
		userNameRow.add(userNameValue);
		userLvlRow.add(lvl);
		userLvlRow.add(lvlValue);
		userWinsRow.add(wins);
		userWinsRow.add(winsValue);
		userLosesRow.add(loses);
		userLosesRow.add(losesValue);
		
				
		userPanel.add(ava);
		userPanel.add(userNameRow);
		userPanel.add(userLvlRow);
		userPanel.add(userWinsRow);
		userPanel.add(userLosesRow);
		

		playerQueue.add(overQueue);

		
		playerQueue.add(userList);
	
		
		overQueue.add(search);
		overQueue.add(clear);

		
		// add style
		container.addStyleName("game");
		clear.addStyleName("button button-clear");
		startGame.addStyleName("button button-start");
		refresh.addStyleName("button button-refresh");
		cancel.addStyleName("button cancel-button");
		userPanel.addStyleName("playerPanel");
		playerQueue.addStyleName("player-queue");
		userList.addStyleName("user-list");
		overQueue.addStyleName("over-queue");
		ava.addStyleName("ava");
		userName.addStyleName("user-name");
		lvl.addStyleName("lvl");
		wins.addStyleName("wins");
		loses.addStyleName("loses");
		
		userNameRow.addStyleName("row row-user-name");
		userLvlRow.addStyleName("row row-lvl");
		userWinsRow.addStyleName("row row-wins");
		userLosesRow.addStyleName("row row-loses");

	}

	@Override
	public void setData(List<User> list) {
		userList.clear();
		for (User u : list){
			userList.addItem(u.getLogin());
		}
	}
	
	@Override
	public TextBox getSearch(){
		return search;
	}
	
	@Override
	public Button getStartGame(){
		return startGame;
	}
	
	@Override
	public HasClickHandlers getClear(){
		return clear;
	}	

	@Override
	public Button getCancel() {
		return cancel;
	}	

	@Override
	public Button getRefresh() {
		return refresh;
	}
	
	@Override
	public void setUserName(String userName) {
	userNameValue.setText(userName);
	}
	
	@Override
	public void setLvl(String lvl) {
	lvlValue.setText(lvl);
	}
	
	@Override
	public void setWins(String wins) {
	winsValue.setText(wins);
	}
	
	@Override
	public void setLoses(String loses) {
	losesValue.setText(loses);
	}
	
	@Override
	public void setAvatar(String url) {
	ava.setUrl(url);
	}

	@Override
	public Widget asWidget() {
		return this;
	}
}
