/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic.state;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class CardState implements Serializable{

	private int position;

	private int currentHealth;

	public CardState() {
		position = -1;
		currentHealth = 0;
	}

	public CardState(CardState cardState) {
		position = cardState.getPosition();
		currentHealth = cardState.getCurrentHealth();
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(int currentHealth) {
		this.currentHealth = currentHealth;
	}	
}
