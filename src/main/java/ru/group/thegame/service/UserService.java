/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import java.util.List;
import ru.group.thegame.domain.User;

/**
 *
 * @author potapov
 */
public interface UserService {
    
    public User register(User entity);

    public List<User> getAll();

    public User getById(long id);

    public void delete(User entity);

    public User save(User entity);
    
    public ru.group.thegame.shared.User init(User enity);

    public User unwrap(ru.group.thegame.shared.User entity);
    
    public User findByLogin(String login);
}
