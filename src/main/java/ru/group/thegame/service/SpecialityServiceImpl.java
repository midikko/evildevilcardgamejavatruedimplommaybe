/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import ru.group.thegame.dao.SpecialityDAO;
import ru.group.thegame.domain.Speciality;

/**
 *
 * @author potapov
 */
public class SpecialityServiceImpl implements SpecialityService{
    
    @Autowired
    private SpecialityDAO specialityDao;
    
    @Override
    public void register(Speciality entity) {
        specialityDao.create(entity);
    }

    @Override
    public List<Speciality> getAll() {
        return specialityDao.findAll();
    }

    @Override
    public Speciality getById(long id) {
        return specialityDao.findByID(id);
    }

    @Override
    public void delete(Speciality entity) {
        specialityDao.delete(entity);
    }

    @Override
    public void save(Speciality entity) {
        specialityDao.save(entity);
    }

    @Override
    public ru.group.thegame.shared.Speciality fromDomainToShared(Speciality entity) {
        ru.group.thegame.shared.Speciality shar = new ru.group.thegame.shared.Speciality();
        shar.setDescr(entity.getDescr());
        shar.setId(entity.getId());
        shar.setName(entity.getName());
        return shar;
    }

    @Override
    public Speciality fromSharedToDomain(ru.group.thegame.shared.Speciality entity) {
        Speciality domain = new Speciality();
        domain.setDescr(entity.getDescr());
        domain.setId(entity.getId());
        domain.setName(entity.getName());
        return domain;
    }

	@Override
	public ru.group.thegame.server.logic.Speciality fromDomainToLogic(Speciality entity) {		
        ru.group.thegame.server.logic.Speciality logic = new ru.group.thegame.server.logic.Speciality();
        logic.setCards(entity.getCards());
        logic.setDescr(entity.getDescr());
        logic.setId(entity.getId());
        logic.setName(entity.getName());
        logic.setUsers(entity.getUsers());
        return logic;
	}

	@Override
	public ru.group.thegame.server.logic.Speciality fromSharedToLogic(ru.group.thegame.shared.Speciality entity) {
        ru.group.thegame.server.logic.Speciality logic = new ru.group.thegame.server.logic.Speciality();
        logic.setDescr(entity.getDescr());
        logic.setId(entity.getId());
        logic.setName(entity.getName());
        return logic;
	}

	@Override
	public ru.group.thegame.shared.Speciality fromLogicToShared(ru.group.thegame.server.logic.Speciality speciality) {
		ru.group.thegame.shared.Speciality shar = new ru.group.thegame.shared.Speciality();
        shar.setDescr(speciality.getDescr());
        shar.setId(speciality.getId());
        shar.setName(speciality.getName());
        return shar;
	}
    
}
