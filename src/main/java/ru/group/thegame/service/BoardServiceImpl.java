/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import ru.group.thegame.shared.Board;
import ru.group.thegame.shared.Card;

public class BoardServiceImpl implements BoardService {

	@Autowired
	CardService cardService;

	@Override
	public Board fromLogicToShared(ru.group.thegame.server.logic.Board board) {
		Board b = new Board();
		List<Card> list = new ArrayList<>();
		for (ru.group.thegame.server.logic.Card card : board.getUserCards(0)) {
			if (card != null) {
				list.add(cardService.fromLogicToShared(card));
			} else {
				list.add(null);
			}
		}
		b.setUser1Cards(list);
		list = new ArrayList<>();

		for (ru.group.thegame.server.logic.Card card : board.getUserCards(1)) {
			if (card != null) {
				list.add(cardService.fromLogicToShared(card));
			} else {
				list.add(null);
			}
		}
		b.setUser2Cards(list);
		return b;
	}

}
