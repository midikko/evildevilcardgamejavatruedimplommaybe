/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.dao;

import java.util.List;
import ru.group.thegame.domain.SpecialEffect;

/**
 *
 * @author potapov
 */
public interface SpecialEffectDAO {
    
    public SpecialEffect create(SpecialEffect entity);

    public SpecialEffect save(SpecialEffect entity);

    public void delete(SpecialEffect entity);

    public List findAll();

    public SpecialEffect findByID(long id);
    
    public boolean isExist(SpecialEffect entity);
}
