/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic.event;

import java.io.Serializable;

/**
 *
 * @author nevezhinpavel
 */
public class CardHealthChangeEvent implements Event, Serializable {
	
	private int owner;
	
	private int position;
	
	private int healthChange;

	public CardHealthChangeEvent(int owner, int position, int healthChange) {
		this.owner = owner;
		this.position = position;
		this.healthChange = healthChange;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getHealthChange() {
		return healthChange;
	}

	public void setHealthChange(int healthChange) {
		this.healthChange = healthChange;
	}
	
	
}
