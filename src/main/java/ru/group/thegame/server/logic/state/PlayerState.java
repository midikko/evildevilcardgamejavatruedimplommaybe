/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic.state;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import ru.group.thegame.server.logic.Card;

/**
 *
 * @author Admin
 */
public class PlayerState implements Serializable{	
	
	private int health;
	
	private List<Integer> powers;
	
	private List<Card> cards;

	public PlayerState() {
		powers = new ArrayList<>();
		health = 25;
		powers.add(2);
		powers.add(2);
		powers.add(2);
		powers.add(2);
		powers.add(1);
	}

	public PlayerState(PlayerState playerState) {
		powers = new ArrayList<>(playerState.getPowers());
		cards = new ArrayList<>(playerState.getCards());
		health = playerState.getHealth();
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public List<Integer> getPowers() {
		return powers;
	}

	public void setPowers(List<Integer> powers) {
		this.powers = powers;
	}

	public List<Card> getCards() {
		return cards;
	}	

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}
}
