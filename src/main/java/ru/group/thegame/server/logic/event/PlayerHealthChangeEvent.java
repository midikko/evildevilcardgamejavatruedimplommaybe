/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic.event;

import java.io.Serializable;

/**
 *
 * @author nevezhinpavel
 */
public class PlayerHealthChangeEvent implements Event, Serializable {
	
	private int player;
	
	private int healthChange;

	public PlayerHealthChangeEvent(int player, int healthChange) {
		this.player = player;
		this.healthChange = healthChange;
	}

	public int getPlayer() {
		return player;
	}

	public void setPlayer(int player) {
		this.player = player;
	}

	public int getHealthChange() {
		return healthChange;
	}

	public void setHealthChange(int healthChange) {
		this.healthChange = healthChange;
	}
}
