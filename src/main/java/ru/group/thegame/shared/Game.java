/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Nevezhin_Pavel
 */
public class Game implements Serializable {

	private long UID;

	private Board board;

	private List<Player> players = new ArrayList<>(2);

	private int turn;

	private int currentUser;
	
	private int time;

	public Game() {
	}

	public long getUID() {
		return UID;
	}

	public void setUID(long UID) {
		this.UID = UID;
	}

	public Board getBoard() {
		return board;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}

	public int getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(int currentUser) {
		this.currentUser = currentUser;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int newTime) {
		time = newTime;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + (int) (this.UID ^ (this.UID >>> 32));
		hash = 53 * hash + this.turn;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Game other = (Game) obj;
		if (this.UID != other.UID) {
			return false;
		}
		if (!Objects.equals(this.board, other.board)) {
			return false;
		}
		if (!Objects.equals(this.players, other.players)) {
			return false;
		}
		if (this.turn != other.turn) {
			return false;
		}
		if (this.currentUser != other.currentUser) {
			return false;
		}
		return true;
	}
}
