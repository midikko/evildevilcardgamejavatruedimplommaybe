/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.dao;

import java.util.List;
import ru.group.thegame.domain.Speciality;

/**
 *
 * @author potapov
 */
public interface SpecialityDAO {
    
    public Speciality create(Speciality entity);

    public Speciality save(Speciality entity);

    public void delete(Speciality entity);

    public List findAll();

    public Speciality findByID(long id);
    
    public boolean isExist(Speciality entity);
}
