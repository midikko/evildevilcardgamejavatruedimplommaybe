/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.HTMLPanel;

/**
 *
 * @author Nevezhin_Pavel
 */
public class TheGame implements EntryPoint {

	@Override
	public void onModuleLoad() {
		GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
			@Override
			public void onUncaughtException(Throwable e) {
				Window.alert(e.getMessage());
			}
		});

		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			@Override
			public void execute() {
				//Создание главного контролера
				HTMLPanel wrap = HTMLPanel.wrap(DOM.getElementById("all"));

				//Создание событийной шины		
				HandlerManager eventBus = new HandlerManager(null);

				ApplicationController appViewer = new ApplicationController(wrap, eventBus);
				//Запуск созданного контролера
				appViewer.go();
			}
		});
	}
}
