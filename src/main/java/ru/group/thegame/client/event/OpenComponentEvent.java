/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.event;

import com.google.gwt.event.shared.GwtEvent;
import java.util.Map;

/**
 *
 * @author nikolaev
 */
public class OpenComponentEvent extends GwtEvent<OpenComponentEventHandler> {

    public static Type<OpenComponentEventHandler> TYPE = new Type<OpenComponentEventHandler>();
    public String target;
    public Map<String, String> parameters;

    public OpenComponentEvent(String target) {
//		super();
        this.target = target;
    }

    @Override
    public Type<OpenComponentEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(OpenComponentEventHandler handler) {
        handler.onOpenComponent(this);
    }

}
