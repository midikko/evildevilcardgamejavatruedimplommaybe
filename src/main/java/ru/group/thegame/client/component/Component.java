/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.client.component;

import ru.group.thegame.client.Presenter;

/**
 *
 * @author nikolaev
 */
public interface Component extends Presenter{
	
	public String getDefaultHistoryToken();	
}
