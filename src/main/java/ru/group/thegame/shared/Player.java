/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.shared;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Nevezhin_Pavel
 */
public class Player implements Serializable {

	private User user;

	private int health;

	List<Integer> powers;

	List<Card> cards;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public List<Integer> getPowers() {
		return powers;
	}

	public void setPowers(List<Integer> powers) {
		this.powers = powers;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void setCards(List<Card> cards) {
		this.cards = cards;
	}

	public Player() {
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 37 * hash + Objects.hashCode(this.user);
		hash = 37 * hash + this.health;
		hash = 37 * hash + Objects.hashCode(this.powers);
		hash = 37 * hash + Objects.hashCode(this.cards);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Player other = (Player) obj;
		if (!Objects.equals(this.user, other.user)) {
			return false;
		}
		if (this.health != other.health) {
			return false;
		}
		if (!Objects.equals(this.powers, other.powers)) {
			return false;
		}
		if (!Objects.equals(this.cards, other.cards)) {
			return false;
		}
		return true;
	}
}
