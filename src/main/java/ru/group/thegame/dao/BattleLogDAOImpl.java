/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.dao;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.hibernate.Hibernate;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import ru.group.thegame.domain.BattleLog;

/**
 *
 * @author potapov
 */
public class BattleLogDAOImpl implements BattleLogDAO{
    
    @Resource(name = "sessionFactory")
    protected SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    protected Session getSession() {
        return sessionFactory.openSession();
    }

    @Override
    public void create(BattleLog entity) {
        Session session = getSession();
        session.save(entity);
        if (session != null) {
            session.flush();
            session.close();
        }
    }

    @Override
    public List findAll() {
        Session session = getSession();
        session.beginTransaction();
        String query = "from BattleLog where del=NULL and id not in(select old_act from BattleLog where old_act=0)";
        List<BattleLog> entitys = new ArrayList<BattleLog>(session.createQuery(query).list());
        session.getTransaction().commit();
        if (session != null) {
            session.flush();
            session.close();
        }
        return entitys;
    }

    @Override
    @Transactional
    public BattleLog save(BattleLog entity) {
        
        Session session = getSession();
		//Update action Details
        //Transaction tx = session.beginTransaction();
        //не знаю что писать, посему жду пояснений
        BattleLog a = new BattleLog(); //это не решение, а заглушка
        session.update(entity);
        //tx.commit();
        if (session != null) {
            session.flush();
            session.close();
        }
        return a;
    }

	@Override
	public void delete(BattleLog entity) {
		Session session = getSession();
        //delete customer Details
		//Transaction tx = session.beginTransaction();
		session.delete(entity);
		//tx.commit();
		if (session != null) {
			session.flush();
			session.close();
		}
	}

	@Override
	public BattleLog findByID(long id) {


        Session session = getSession();
        BattleLog entity = (BattleLog) session.load(BattleLog.class, id);
        try {
            Hibernate.initialize(entity);
        } catch (ObjectNotFoundException e) {
            System.out.println("BattleLog object with id " + id + " not found");
            entity = null;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }
        return entity;
	}

	@Override
	public boolean isExist(BattleLog entity) {

		Session session = getSession();
		BattleLog entity1 = findByID(entity.getId());
		if (entity1 == null) {
			return false;
		};
		return true;
	}
}
