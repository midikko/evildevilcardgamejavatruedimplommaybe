/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import java.util.List;
import ru.group.thegame.domain.Speciality;

/**
 *
 * @author potapov
 */
public interface SpecialityService {
    
    public void register(Speciality entity);

    public List<Speciality> getAll();

    public Speciality getById(long id);

    public void delete(Speciality entity);

    public void save(Speciality entity);
    
    public ru.group.thegame.shared.Speciality fromDomainToShared(Speciality enity);

    public Speciality fromSharedToDomain(ru.group.thegame.shared.Speciality entity);
    
    public ru.group.thegame.server.logic.Speciality fromDomainToLogic(Speciality enity);

    public  ru.group.thegame.server.logic.Speciality fromSharedToLogic(ru.group.thegame.shared.Speciality entity);

	public ru.group.thegame.shared.Speciality fromLogicToShared(ru.group.thegame.server.logic.Speciality speciality);
    
}
