/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.component.game.presenter;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import java.util.List;
import ru.group.thegame.client.Presenter;
import ru.group.thegame.client.component.game.GameRPCService;
import ru.group.thegame.client.component.game.GameRPCServiceAsync;
import ru.group.thegame.client.event.OpenComponentEvent;
import ru.group.thegame.shared.Board;
import ru.group.thegame.shared.Card;
import ru.group.thegame.shared.Game;
import ru.group.thegame.shared.Player;
import ru.group.thegame.shared.User;

/**
 *
 * @author MasterKrab
 */
public class GamePresenter implements Presenter {

	public interface Display {

		public long getId();

		public Button getSurrender();

		public Button getSkip();

		public Button getRefresh();

		public List<FocusPanel> getCards();

		public List<FocusPanel> getMyCards();

		public void setData(Game game);

		public void setTimer(String time);

		public void setCurrentUser(long currentUser);

		public Widget asWidget();
	}

	private final Display display;
	private final HandlerManager eventBus;
	private final GameRPCServiceAsync rpcService = GWT.create(GameRPCService.class);

	private Game game;
	private User user;
	private Card selected;
	private int currentUser = -1; // id в игре (0 или 1) пользователя в игре
	private int time;

	public GamePresenter(HandlerManager eventBus, Display display) {
		this.display = display;
		this.eventBus = eventBus;
	}

	@Override
	public void go(HasWidgets container) {
		bindOnce();
		startTimer();
		getCurrentUser();
		container.clear();
		container.add(display.asWidget());
	}

	private void bindOnce() {
		display.getSkip().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				rpcService.skipTurn(new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
					}

					@Override
					public void onSuccess(Void result) {
						getGame();
					}
				});
			}
		});

		display.getRefresh().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				getGame();
			}
		});

		display.getSurrender().addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				rpcService.surrender(display.getId(), new AsyncCallback<Void>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert(this.getClass().toString() + "\n\nbind()=>surrender fails");
					}

					@Override
					public void onSuccess(Void result) {
						Window.alert("YOU LOSE!");
						eventBus.fireEvent(new OpenComponentEvent("game-mainmenu"));
					}
				});
			}
		});
	}

	private void bind() {
		for (int i = 0; i < display.getCards().size(); i++) {
			final FocusPanel fp = display.getCards().get(i);
			final int j = i;
			fp.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					for (FocusPanel fp : display.getCards()) {
						fp.removeStyleName("selected");
					}
					fp.addStyleName("selected");
					selected = game.getPlayers().get(currentUser).getCards().get(j);
				}
			});
			fp.addMouseOverHandler(new MouseOverHandler() {

				@Override
				public void onMouseOver(MouseOverEvent event) {
					fp.addStyleName("in-focus");
				}
			});
			fp.addMouseOutHandler(new MouseOutHandler() {

				@Override
				public void onMouseOut(MouseOutEvent event) {
					fp.removeStyleName("in-focus");
				}
			});
		}

		for (int i = 0; i < display.getMyCards().size(); i++) {
			final FocusPanel fp = display.getMyCards().get(i);
			final int j = i;
			fp.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					List<Card> userCards = currentUser == 0 ? game.getBoard().getUser1Cards() : game.getBoard().getUser2Cards();
					List<Integer> powers = game.getPlayers().get(game.getCurrentUser()).getPowers();
					int power;
					if (selected.getSpeciality().getId() < 4) {
						power = (int) selected.getSpeciality().getId();
					} else {
						power = 4;
					}
					if (selected != null && currentUser == game.getCurrentUser() && userCards.get(j) == null && !(powers.get(power) < selected.getCost())) {
						rpcService.putCard(selected, j, new AsyncCallback<Void>() {

							@Override
							public void onFailure(Throwable caught) {
								throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
							}

							@Override
							public void onSuccess(Void result) {
								getGame();
							}
						});
					}
				}
			});
		}
	}

	private void getGame() {
		if (selected != null) {
			List<Card> cards = game.getPlayers().get(currentUser).getCards();
			for (int i = 0; i < cards.size(); i++) {
				if (cards.get(i).equals(selected)) {
					display.getCards().get(i).addStyleName("selected");
				}
			}
		}
		long id = display.getId();
		rpcService.getGameByUID(id, new AsyncCallback<Game>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert(this.getClass().toString() + "\n\ngetGame()=>getGameById fails");
			}

			@Override
			public void onSuccess(Game result) {
				if (result != null) {
					if (result.equals(game)) {
						return;
					}

					if (game != null) {
						Window.alert("game not null");
						Board oldBoard = game.getBoard();
						Board newBoard = result.getBoard();

						if (!newBoard.equals(oldBoard)) {
							Window.alert("boards not equals");
							List<Card> oldUser1Cards = oldBoard.getUser1Cards();
							List<Card> newUser1Cards = newBoard.getUser1Cards();
							if (!oldUser1Cards.equals(newUser1Cards)) {
								Window.alert("userCards not equals");
								for (int i = 0; i < 6; i++) {
									Card oldGet = oldUser1Cards.get(i);
									Card newGet = newUser1Cards.get(i);
									if (oldGet == null || newGet == null){
										continue;
									}
									if (!(oldGet.getCurrentHealth() == newGet.getCurrentHealth())) {
										Window.alert("CurrentHealth not equals");
										Window.alert(oldGet.getName() + "\nOld health = " + oldGet.getCurrentHealth() + "\nNew health = " + newGet.getCurrentHealth());
									}
								}
							}
							List<Card> oldUser2Cards = oldBoard.getUser2Cards();
							List<Card> newUser2Cards = newBoard.getUser2Cards();
							if (!oldUser2Cards.equals(newUser2Cards)) {
								for (int i = 0; i < 6; i++) {
									Card oldGet = oldUser2Cards.get(i);
									Card newGet = newUser2Cards.get(i);
									if (oldGet == null || newGet == null){
										continue;
									}
									if (!(oldGet.getCurrentHealth() == newGet.getCurrentHealth())) {
										Window.alert(oldGet.getName() + "\nOld health = " + oldGet.getCurrentHealth() + "\nNew health = " + newGet.getCurrentHealth());
									}
								}
							}
						}

						List<Player> oldPlayers = game.getPlayers();
						List<Player> newPlayers = result.getPlayers();
						if (oldPlayers.equals(newPlayers)) {
							for (int i = 0; i < 2; i++) {
								Player oldGet = oldPlayers.get(i);
								Player newGet = newPlayers.get(i);
								if (!(oldGet.getHealth() == newGet.getHealth())) {
									Window.alert(oldGet.getUser().getLogin() + "\nOld health = " + oldGet.getHealth() + "\nNew health = " + newGet.getHealth());
								}
							}
						}
					}
					game = result;
					for (int i = 0; i < 2; i++) {
						if (game.getPlayers().get(i).getUser().equals(user)) {
							currentUser = i;
							display.setCurrentUser(i);
						}
					}
					time = result.getTime();
					display.setData(result);
					bind();
					if (currentUser != game.getCurrentUser()) {
						display.getSkip().setEnabled(false);
					} else {
						display.getSkip().setEnabled(true);
					}
				} else {
					eventBus.fireEvent(new OpenComponentEvent("game-mainmenu"));
				}
			}
		});
	}

	private void getCurrentUser() {
		rpcService.getCurrentUser(new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {
				throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
			}

			@Override
			public void onSuccess(User result) {
				user = result;
				getGame();
			}
		});
	}

	private void startTimer() {
		final Timer timer = new Timer() {
			@Override
			public void run() {
				if (game == null) {
					cancel();
				}
				if (time == 0) {
					getGame();
				}
				if (time <= 0) {
					display.setTimer("0");
				} else {
					display.setTimer((int) (time / 2) + "");
				}
				time -= 2;
			}
		};
		timer.scheduleRepeating(1000);
		Timer t = new Timer() {

			@Override
			public void run() {
				if (game == null) {
					cancel();
				}
				getGame();
			}
		};
		t.scheduleRepeating(10000);
	}
}
