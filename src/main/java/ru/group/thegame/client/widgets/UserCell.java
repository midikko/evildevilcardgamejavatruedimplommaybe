/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.widgets;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.Cell.Context;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Image;
import ru.group.thegame.shared.User;

/**
 *
 * @author Admin
 */
public class UserCell extends AbstractCell<User> {

	/**
	 * The html of the image used for contacts.
	 */
//	private final String imageHtml;

	public UserCell() {
//		this.imageHtml = AbstractImagePrototype.create(image).getHTML();
	}

	@Override
	public void render(Context context, User value, SafeHtmlBuilder sb) {
		// Value can be null, so do a null check..
		if (value == null) {
			return;
		}

		sb.appendHtmlConstant("<table>");

		// Add the contact image.
		sb.appendHtmlConstant("<tr><td rowspan='3'>");
//		sb.appendHtmlConstant(imageHtml);
		sb.appendHtmlConstant("</td>");

		// Add the name and address.
		sb.appendHtmlConstant("<td style='font-size:95%;'>");
		sb.appendEscaped(value.getLogin());
		sb.appendHtmlConstant("</td></tr><tr><td>");
		sb.appendEscaped(value.getRank() + "");
		sb.appendHtmlConstant("</td></tr></table>");
	}
}
