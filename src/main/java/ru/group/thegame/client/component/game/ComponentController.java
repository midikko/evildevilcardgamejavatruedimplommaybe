/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.component.game;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasWidgets;
import ru.group.thegame.client.Presenter;
import ru.group.thegame.client.component.Component;
import ru.group.thegame.client.component.game.view.MainMenuView;

/**
 *
 * @author MasterKrab
 */
public class ComponentController implements Component, ValueChangeHandler<String> {

	private HasWidgets container;
	private final HandlerManager eventBus;

	public ComponentController(HandlerManager eventBus) {
		this.eventBus = eventBus;
	}

	@Override
	public void go(HasWidgets container) {
		this.container = container;
		openPage(getDefaultHistoryToken());
	}

	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		openPage(event.getValue());
	}

	private void openPage(String token) {
		if (token != null) {
			Presenter presenter = null;
			String[] split = token.split("-");
			if (split[0].equals("game")) {
				switch (split[1]) {
					case "game":
						presenter = new ru.group.thegame.client.component.game.presenter.GamePresenter(eventBus, new ru.group.thegame.client.component.game.view.GameView(Long.parseLong(split[2])));
						break;
					case "mainmenu":
						presenter = new ru.group.thegame.client.component.game.presenter.MainMenuPresenter(new MainMenuView(), eventBus);
						break;
				}
				if (presenter != null) {
					presenter.go(container);
				}
			}
		}
	}

	@Override
	public String getDefaultHistoryToken() {
		return "game-mainmenu";
	}
}
