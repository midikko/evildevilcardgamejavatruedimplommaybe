/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.client;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 *
 * @author nikolaev
 */
public interface Presenter {
	
	public void go(final HasWidgets container);
	
}
