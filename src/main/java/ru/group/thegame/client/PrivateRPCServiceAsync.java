/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author Nevezhin_Pavel
 */
public interface PrivateRPCServiceAsync {
	
	public void isAuthenticated(AsyncCallback<Boolean> callback);
	
	public void isRoot(AsyncCallback<Boolean> callback);
}
