/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.rpc;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.group.thegame.Initiator;
import ru.group.thegame.client.component.game.GameRPCService;
import ru.group.thegame.server.logic.BoardManager;
import ru.group.thegame.server.logic.Game;
import ru.group.thegame.server.logic.GameManager;
import ru.group.thegame.server.logic.Player;
import ru.group.thegame.service.CardService;
import ru.group.thegame.service.UserService;
import ru.group.thegame.service.GameService;
import ru.group.thegame.shared.Card;
import ru.group.thegame.shared.User;

@Service("GameRPCService")
public class GameRPCServiceImpl implements GameRPCService {

	@Autowired
	UserService userService;

	@Autowired
	CardService cardService;

	@Autowired
	GameService gameService;

	@Autowired
	GameManager GM;

	@Override
	public List<User> getOnlineUsers() {
		List<User> list = new ArrayList<>();
		for (ru.group.thegame.domain.User u : GM.getOnlineUsers()) {
			list.add(userService.init(u));
		}
		return list;
	}

	@Override
	public void startGame() {
		org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		ru.group.thegame.domain.User u = userService.findByLogin(user.getUsername());
		GM.wannaplay(u);

	}

	@Override
	public List<Card> getCommonCards() {
		List<Card> list = new ArrayList<>();
		for (ru.group.thegame.domain.Card card : cardService.getCommon()) {
			list.add(cardService.fromDomainToShared(card));
		}
		return list;
	}

	@Override
	public Long amIPlaying() {
		User currentUser = getUser();
		for (Game game : GM.getGames()) {
			for (Player player : game.getPlayers()) {
				if (player.getUser().getLogin().equals(currentUser.getLogin())) {
					return game.getUID();
				}
			}
		}
		return -1l;
	}

	@Override
	public void leaveQueue() {
		GM.removeFromQueue(userService.unwrap(getUser()));
	}

	@Override
	public ru.group.thegame.shared.Game getGameByUID(long uid) {
		ru.group.thegame.shared.Game g = null;
		for (Game game : GM.getGames()) {
			if (game.getUID() == uid) {
				g = gameService.fromLogicToShared(game);
			}
		}
		return g;
	}

	private User getUser() {
		org.springframework.security.core.userdetails.User u = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = userService.init(userService.findByLogin(u.getUsername()));
		return user;
	}

	@Override
	public void surrender(long uid) {
		for (Game game : GM.getGames()) {
			if (game.getUID() == uid) {
				game.surrender();
			}
		}
	}

	@Override
	public User getCurrentUser() {
		return getUser();
	}

	@Override
	public void putCard(Card selected, int j) {
		for (Game game : GM.getGames()) {
			for (Player player : game.getPlayers()) {
				if (player.getUser().getLogin().equals(getUser().getLogin())) {
					if (player.getUser().getLogin().equals(game.getPlayers().get(game.getGameState().getCurrentUser()).getUser().getLogin())) {
						if (Initiator.DEBUG) {
							System.out.println("\nI put card " + selected.getName() + "\n");
							System.out.println("\n\nDEBUG Board when serger get card  IN" + this.getClass().getName() + "::" + game.getBoard() + "\n");
						}
						ru.group.thegame.server.logic.Card fromSharedToLogic = cardService.fromSharedToLogic(selected);
						fromSharedToLogic.setBoardManager(new BoardManager(game.getBoard()));
						game.putCard(j, fromSharedToLogic);
					}
					return;
				}
			}
		}
	}

	@Override
	public void skipTurn() {
		for (Game game : GM.getGames()) {
			for (Player player : game.getPlayers()) {
				if (player.getUser().getLogin().equals(getUser().getLogin())) {
					if (player.getUser().getLogin().equals(game.getPlayers().get(game.getGameState().getCurrentUser()).getUser().getLogin())) {
						if (Initiator.DEBUG) {
							System.out.println("\nI skip turn\n");
						}
						game.skip();
					}
					return;
				}
			}
		}
	}
}
