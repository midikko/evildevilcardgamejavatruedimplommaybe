/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import ru.group.thegame.shared.Game;

/**
 *
 * @author Nevezhin_Pavel
 */
public interface GameService {
	
	public Game fromLogicToShared(ru.group.thegame.server.logic.Game game);
	 
}
