/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.dao;

import java.util.List;
import ru.group.thegame.domain.BattleLog;

/**
 *
 * @author potapov
 */
public interface BattleLogDAO {
    public void create(BattleLog entity);

    public BattleLog save(BattleLog entity);

    public void delete(BattleLog entity);

    public List findAll();

    public BattleLog findByID(long id);

    public boolean isExist(BattleLog entity);
}
