/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import ru.group.thegame.server.logic.Player;
import ru.group.thegame.shared.Game;


public class GameServiceImpl implements GameService {
	
	@Autowired
	BoardService boardService;
	
	@Autowired
	PlayerService playerService;

	@Override
	public Game fromLogicToShared(ru.group.thegame.server.logic.Game game) {
		Game g = new Game();
		g.setBoard(boardService.fromLogicToShared(game.getBoard()));
		g.setCurrentUser(game.getGameState().getCurrentUser());
		List<ru.group.thegame.shared.Player> list = new ArrayList<>();
		for (Player p : game.getPlayers()){
			list.add(playerService.fromLogicToShared(p));
		}
		g.setTime(game.getTime());
		g.setPlayers(list);
		g.setTurn(game.getGameState().getTurn());
		g.setUID(game.getUID());
		return g;
	}
	
}
