/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.client.event;

import com.google.gwt.event.shared.EventHandler;

/**
 *
 * @author nikolaev
 */
public interface OpenComponentEventHandler extends EventHandler{
	void onOpenComponent( OpenComponentEvent event );
}
