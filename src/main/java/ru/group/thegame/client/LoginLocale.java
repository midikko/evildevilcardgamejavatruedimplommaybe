/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client;

import java.util.Locale;

/**
 *
 * @author potapov
 */
public class LoginLocale {

    public static String getLogin(Locale locale) {
        String language = locale.getLanguage();
        if(language.equalsIgnoreCase("ru")){
            return "Вход";
        }
        if(language.equalsIgnoreCase("en")){
            return "Login";
        }
        return "Login";
    }

    public static String getPassword(Locale locale) {
        String language = locale.getLanguage();
        if(language.equalsIgnoreCase("ru")){
            return "Пароль :";
        }
        if(language.equalsIgnoreCase("en")){
            return "Password :";
        }
        return "Password :";
    }

    public static String getUsername(Locale locale) {
        String language = locale.getLanguage();
        if(language.equalsIgnoreCase("ru")){
            return "Логин :";
        }
        if(language.equalsIgnoreCase("en")){
            return "Username :";
        }
        return "Username :";
    }

    public static String getRemember(Locale locale) {
        String language = locale.getLanguage();
        if(language.equalsIgnoreCase("ru")){
            return "Запомнить меня ";
        }
        if(language.equalsIgnoreCase("en")){
            return "Remember me on this computer ";
        }
        return "Remember me on this computer ";
    }
    
        public static String getSubmit(Locale locale) {
        String language = locale.getLanguage();
        if(language.equalsIgnoreCase("ru")){
            return "Войти";
        }
        if(language.equalsIgnoreCase("en")){
            return "Login";
        }
        return "Login";
    }
}
