/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.hibernate.Hibernate;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import ru.group.thegame.domain.User;

/**
 *
 * @author potapov
 */
@Transactional
public class UserDAOImpl implements UserDAO {

    @Resource(name = "sessionFactory")
    public SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Session getSession() {
        return sessionFactory.openSession();
    }

    @Override
    public User create(User user) {
        Session session = getSession();
		long id = (long) session.save(user);
        if (session != null) {
            session.flush();
            session.close();
        }
		return findByID(id);
    }

    @Override
    public List findAll() {
        Session session = getSession();
        session.beginTransaction();
        List<User> users = new ArrayList<User>(session.createQuery("from User").list());
        session.getTransaction().commit();
        if (session != null) {
            session.flush();
            session.close();
        }
        return users;
    }

    @Override
    @Transactional
    public User save(User user) {
        
        Session session = getSession();
        //Update user Details
        //Transaction tx = session.beginTransaction();
        session.update(user);
        //tx.commit();
        if (session != null) {
            session.flush();
            session.close();
        }
		return findByID(user.getId());
    }

    @Override
    public void delete(User user) {
        Session session = getSession();
        //delete user Details
        //Transaction tx = session.beginTransaction();
        session.delete(user);
        //tx.commit();
        if (session != null) {
            session.flush();
            session.close();
        }
    }

    @Override
    public User findByID(long id) {

        Session session = getSession();
        User user = (User) session.load(User.class, id);
        try {
            Hibernate.initialize(user);
        } catch (ObjectNotFoundException e) {
            System.out.println("user object with id " + id + " not found");
            user = null;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }

        return user;
    }

    @Override
    public boolean isExist(User user) {
        Session session = getSession();
        User user1 = findByID(user.getId());;
        if (user1 == null) {
            return false;
        };
        return true;
    }

    @Override
    public User findByLogin(String login) {

        Session session = getSession();
        Query query = session.createQuery("from User where login = :login ");
        query.setParameter("login", login);
        
        List<?> list = query.list();

        User user = (User)list.get(0);
        try {
            Hibernate.initialize(user);
        } catch (ObjectNotFoundException e) {
            System.out.println("user object with id " + login + " not found");
            user = null;
        } finally {
            if (session != null) {
                session.flush();
                session.close();
            }
        }

        return user;
    }
}
