/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 *
 * @author Nevezhin_Pavel
 */
@RemoteServiceRelativePath("springGwtServices/PrivateRPCService")
public interface PrivateRPCService extends RemoteService {
	
	public boolean isAuthenticated();
	
	public boolean isRoot();
	
}
