/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.server.logic;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.group.thegame.Initiator;
import ru.group.thegame.domain.BattleLog;
import ru.group.thegame.domain.User;
import ru.group.thegame.service.BattleLogService;

/**
 *
 * @author Nevezhin_Pavel
 */
public class GameManager implements Runnable {

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private GameInitiator GI;
	
	@Autowired
	BattleLogService battleLogService;

	boolean flag = true;

	public GameManager() {
	}

	private BlockingQueue<Game> games;
	private BlockingQueue<User> rtpQueue;
	private BlockingQueue<User> onlineUsers;
	private AtomicLong UID = new AtomicLong();

	public void init() {
		rtpQueue = new LinkedBlockingDeque<>();
		games = new LinkedBlockingDeque<>();
		onlineUsers = new LinkedBlockingDeque<>();
		UID = new AtomicLong(0); // todo значение из базы
	}

	public void wannaplay(User user) {
		if (!rtpQueue.contains(user)) {
			rtpQueue.offer(user);
			flag = !flag;
			if (flag) {
				((Thread) appContext.getBean("gameThread")).start();
			}
		}
	}

	@Override
	public void run() {
		User user1 = rtpQueue.poll();
		User user2 = rtpQueue.poll();
		startGame(user1, user2);
	}

	public void removeFromQueue(User user) {
		flag = !flag;
		rtpQueue.remove(user);
	}

	/**
	 * <p>
	 * Старт игры</p>
	 * <p>
	 * Еще не известно как, берутся 2 пользователя, создаются их игроки,
	 * создается игра и добавляется в список игр</p>
	 */
	private void startGame(User user1, User user2) {
		//todo очередь пользователей или список готовых к игре из которых выбирается 2
		Player player1 = new Player(user1);
		Player player2 = new Player(user2);
		Game game = GI.letusplay(player1, player2);
		UID.incrementAndGet();
		games.add(game);
		game.start();
	}

	/**
	 * <p>
	 * Завершение игры</p>
	 * <p>
	 * Удаление игры из списка текущих и сохранение ее лога в базу</p>
	 *
	 * @param game Завершившаяся игра
	 * @param log Ее лог
	 */
	public void endGame(Game game, BattleLog log) {
	battleLogService.register(log);
		if(Initiator.DEBUG){System.out.println("\n======\nLog for game "+game.getUID()+"was created\n======\n");
							System.out.println(log.getLog());}
		games.remove(game);
	}

	/**
	 * <p>
	 * Возвращение списка игр</p>
	 * <p>
	 * Возвращает список текущих игр</p>
	 *
	 * @return List< Games > о, которых знает GameManager
	 */
	public List<Game> getGames() {
		return new ArrayList<>(games);
	}

	/**
	 * <p>
	 * Возвращение UID</p>
	 * <p>
	 * GameManager нумерует стартуюшие игры, для их индефикации</p>
	 *
	 * @return long значение атомарной переменной, отвечающей за номер игры
	 */
	public long getUID() {
		return UID.get();
	}

	public List<User> getUsers() {
		return new ArrayList<>(rtpQueue);
	}

	public List<User> getOnlineUsers() {
		return new ArrayList<>(rtpQueue);
	}
}
