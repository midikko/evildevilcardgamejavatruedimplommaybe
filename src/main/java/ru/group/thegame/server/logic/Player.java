/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.server.logic;

import ru.group.thegame.domain.User;
import ru.group.thegame.server.logic.state.PlayerState;

/**
 *
 * @author Nevezhin_Pavel
 */
public class Player {
	
	private final User user;
	
	private final PlayerState playerState;

	public User getUser() {
		return user;
	}	

	public Player(User user) {
		this.user = user;
		playerState = new PlayerState();
	}	

	public PlayerState getPlayerState() {
		return playerState;
	}
}
