/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.client.component.menu;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasWidgets;
import java.util.ArrayList;
import java.util.List;
import ru.group.thegame.client.Presenter;
import ru.group.thegame.client.event.OpenComponentEvent;

/**
 *
 * @author MasterKrab
 */
public class ComponentController implements Presenter {

	List<Button> menuItems = new ArrayList<>();
	private final HandlerManager eventBus;

	public ComponentController(HandlerManager eventBus) {
		this.eventBus = eventBus;
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		init();
		for (Button b : menuItems) {
			container.add(b);
		}
	}

	private void init() {
		menuItems.add(new Button("Home", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new OpenComponentEvent("site-home"));
			}
		}));
		menuItems.add(new Button("About", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new OpenComponentEvent("site-about"));
			}
		}));
		menuItems.add(new Button("Game", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new OpenComponentEvent("game"));
			}
		}));
		menuItems.add(new Button("GameMenu", new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new OpenComponentEvent("game-mainmenu"));
			}
		}));

		menuItems.add(new Button("logout", new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Window.Location.replace("TheGame/j_spring_security_logout");
			}
		}));
	}
}
