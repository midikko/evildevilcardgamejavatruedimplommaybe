/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.group.thegame.service;

import ru.group.thegame.shared.Player;

/**
 *
 * @author Nevezhin_Pavel
 */
public interface PlayerService {

	public Player fromLogicToShared(ru.group.thegame.server.logic.Player p);
	
}
