/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.group.thegame.service;

import java.util.List;
import ru.group.thegame.domain.Card;
import ru.group.thegame.domain.Speciality;
import ru.group.thegame.shared.Player;

/**
 *
 * @author potapov
 */
public interface CardService {

	public void register(Card entity);

	public List<Card> getAll();

	public List<Card> getCommon();

	public List<Card> getBySpeciality(Speciality speciality);

	public Card getById(long id);

	public void delete(Card entity);

	public void save(Card entity);

	public ru.group.thegame.shared.Card fromDomainToShared(Card enity);

	public Card fromSharedToDomain(ru.group.thegame.shared.Card entity);

	public ru.group.thegame.server.logic.Card fromDomainToLogic(Card enity);

	public ru.group.thegame.server.logic.Card fromSharedToLogic(ru.group.thegame.shared.Card entity);

	public ru.group.thegame.shared.Card fromLogicToShared(ru.group.thegame.server.logic.Card card);
}
