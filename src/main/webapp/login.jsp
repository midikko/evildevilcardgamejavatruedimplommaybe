<%-- 
    Document   : login
    Created on : 23.08.2014, 7:36:19
    Author     : MasterKrab
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Login</title>    
    <style>
        body {background: #ddd;}
        form { font-family: sans; font-size: 13px;
     
        }
.auth {
width: 252px;
background: #fff;
margin: 10px auto;
padding: 0px 20px;  
padding-bottom: 10px;
border-bottom: 3px solid #aaa;
box-shadow:
0 1px 4px rgba(0, 0, 0, .3),
 -20px 15px 20px -20px rgba(0, 0, 0, .8),
23px 0 20px -23px rgba(0, 0, 0, .8),
0 0 40px rgba(0, 0, 0, .1) inset;
}

.auth h2 {
    padding:  5px 21px;
    background: #00f;
    margin-left: -20px;
    width: 250px;
    margin-bottom: -7px;
    background: #126cae;
    color: #fff;
    border-bottom: 2px solid #666;
} 

.auth label {width: 75px; float: left; padding-top: 5px;}

.auth input {
        padding: 5px 9px;
    border: 1px solid #CCC;
    border-bottom-color: #B3B3B3;
    border-radius: 4px;
    box-shadow: inset 1px 1px #F1F1F1, 0 1px 2px rgba(0,0,0,0.1);
    color: #333;
    font: 14px Arial, sans-serif;
}

input[type="text"], input[type="password"] {width: 145px;} 
input[type="checkbox"] {float: left;}
span {padding-top: 2px; float: left;}

input[type="submit"] {background: #21B384; border-radius: 0px; border: none;
  padding: 2px 20px;
    border: none;
    background-position: left top;
    background-repeat: no-repeat;
    line-height: 32px;
    padding-bottom: 2px;
    color: #fff;
    text-align: center;
    margin-left: 6px;
    cursor: pointer;
    margin: 0 auto;
    margin-left: 50px;
width: 150px;

}

.error {background: red; padding: 4px 16px;}
.error:empty {display: none;}
   </style> 
  </head>
  <body>
      <div class="auth">
    <h2><%= ru.group.thegame.client.LoginLocale.getLogin(request.getLocale()) %></h2><br>
    <form method="POST" action="j_spring_security_check">
        <div class="error"><%= ru.group.thegame.client.LoginFailureHandler.authHandl(request.getParameter("loginFailed")) %></div>
        <label><%= ru.group.thegame.client.LoginLocale.getUsername(request.getLocale()) %></label> <input type="text" name="j_username"><br><br>
        <label><%= ru.group.thegame.client.LoginLocale.getPassword(request.getLocale()) %></label> <input type="password" name="j_password"><br><br>
      <input type="checkbox" name="_spring_security_remember_me"><span><%= ru.group.thegame.client.LoginLocale.getRemember(request.getLocale()) %></span><br><br>
      <input type="submit" value="<%= ru.group.thegame.client.LoginLocale.getSubmit(request.getLocale()) %>">
    </form>
    </div>
  </body>
</html>
